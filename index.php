<?php
/*
Plugin Name: JDS Plugin Foundation
Plugin URI: http://jeremiahsturgill.com
Description: A shared codebase for plugins.
Version: 1.0
Author: J. Sturgill Designs
Author URI: http://jeremiahsturgill.com
License: LimitedUse
*/

// Set up namespace information
namespace JDSPF;
use JDSPF\Core\Functions;
use JDSPF\Core\Models\Core as Core;
function setup_foundation_core()
{
    $foundation_path = dirname(__FILE__) . '/';
    $wp_plugin_path = realpath( $foundation_path . '..' ) . '/';
    require_once( $foundation_path . 'models/core.php' );
    require_once( $foundation_path . 'models/plugin.php' );
    $foundation_folder = basename( $foundation_path ) . '/';
    Core::initialize( $foundation_path, $wp_plugin_path, $foundation_folder );
}
setup_foundation_core();
add_action( 'activated_plugin', '\JDSPF\Core\Models\Core::set_plugin_load_order' );