<?php
namespace JDSPF\Core\Models;
use JDSPF\Core\Functions;
class Entity {

    public $setting_blacklist = array();
    public $name;
    public $html;
    const RELATIVE_TEMPLATE_PATH = 'views/';
    
	public static function string_to_slug($string)
	{
		$string = preg_replace('/[\W]+/i', "_", $string); // convert everything that isn't wordy to _
		$string = strtolower(str_replace('__', '_', $string)); // ensure no more than one _ in a row
		return $string;
	}
    
    protected function sanitize_string($input) 
	{
		return $output = htmlspecialchars($input, ENT_QUOTES | ENT_HTML5 | ENT_DISALLOWED, 'UTF-8');
	}
    
    protected function template_path()
    {
        $file = $this->template.'.php';
        $path=$this->plugin_path.$this::RELATIVE_TEMPLATE_PATH . $this::TEMPLATE_PATH;
        if(file_exists("{$path}/{$file}")) return "{$path}/{$file}";
        // echo realpath($path)."/{$file} does not exist.<br ><br />";

        if($this::OBJECT_VARIABLE_NAME == 'field')
        {
            $path = $this->plugin_path . $this::RELATIVE_TEMPLATE_PATH . 'fields/';
            if(file_exists("{$path}{$file}")) return "{$path}{$file}";
            //echo realpath($path)."/{$file} does not exist.<br ><br />";
        }

        $path = $this->core_path . $this::RELATIVE_TEMPLATE_PATH . $this::TEMPLATE_PATH;
        if(file_exists("{$path}/{$file}")) return "{$path}/{$file}";
        // echo realpath($path)."/{$file} does not exist.<br ><br />";
        
        if($this::OBJECT_VARIABLE_NAME == 'field')
        {
            $path = $this->core_path . $this::RELATIVE_TEMPLATE_PATH . 'fields/';
            if(file_exists("{$path}{$file}")) return "{$path}{$file}";
            // echo realpath($path)."/{$file} does not exist.<br ><br />";
        }

        echo "No template found for {$this->template}.";
    }
    
    protected function load_template($vars=null)
    {
        $array = array($this::OBJECT_VARIABLE_NAME=>$this);
        extract($array);
        if (is_array($vars))
        {
            extract($vars);
        }
        ob_start();
            if (file_exists( $this->template_path() ) )
            {
                include($this->template_path());
            }
            $result = ob_get_contents();
        ob_end_clean();
        if ( $this::OBJECT_VARIABLE_NAME === 'field' )
        {
            $result = $this->wrap_field_template( $result );
        }
        return $result;
    }
    // TODO: transition to having all scripts loaded by register_js
    // ( see: register_filter, register_thickbox, register_slider, register_datepicker, etc. )
    function register_js( $name )
    {
		$object = $this;
		$callback = function() use ( $name, $object )
		{
            if ( $object->is_correct_page() ) 
            {
    			$builtin_path = $object->core_path.'js/jds_core/'.$name.'.js';
    			$builtin_url = plugins_url().'/'.$object->core_folder.'js/jds_core/'.$name.'.js';
    			$plugin_path = $object->plugin_path.'js/'.$name.'.js';
    			$plugin_url = plugins_url().'/'.$object->plugin_folder.'js/'.$name.'.js';
    			if( file_exists( $plugin_path ) )
    			{
    				wp_enqueue_script( 'wpf_' . $name , $plugin_url);
    			} elseif ( file_exists( $builtin_path ) ) {
    				wp_enqueue_script( 'wpf_' . $name , $builtin_url);
    			} elseif ( wp_script_is( $name, $list = 'registered' ) )
    			{
    				wp_enqueue_script( $name );
    			}
            }
		};
		add_action('admin_enqueue_scripts', $callback); 
    }
    
    function render_js_snippet($snippet, $variables)
    {
        extract($variables);
        $relative = 'views/js_snippets/'.$snippet.'.php';
        if(file_exists($this->plugin_path.$relative))
        {
            include($this->plugin_path.'views/js_snippets/'.$snippet.'.php');
        } elseif(file_exists($this->core_path.$relative)) {
            include($this->core_path.'views/js_snippets/'.$snippet.'.php');
        }
    }
    
    public function draw($parent_key = NULL)
    {
        if ( is_numeric( $parent_key ) ) {
            $this->parent_key = $parent_key;
        }
        $path=$this->plugin_path . $this::RELATIVE_TEMPLATE_PATH . $this::TEMPLATE_PATH . 'html/'.$this->name.'.php';
        $this->html = (file_exists($path)) ? file_get_contents($path) : $this->html;
        $content = $this->load_template();
        $html_string = str_replace('[content]', $content, $this->html, $count);
        echo $html_string = ($count == 0) ? $html_string . $content : $html_string; // append to end of string if not inserted
    }
    function register_filter($array)
    {
        $object = $this;
        if($object->is_correct_page())
        {
            add_filter($array['Type'], function($content) use ($object, $array) 
            {
                if($object->is_correct_page())
                {
                    return $array['Function']($content, $object);
                }
            });
        }
    }
    public function register_thickbox()
    {
        if($this->is_correct_page())
        {
            wp_enqueue_script('jquery');
            wp_enqueue_script('thickbox');
            wp_enqueue_style('thickbox');
        }
    }
    public function register_colorpicker()
    {
        if($this->is_correct_page())
        {
            wp_enqueue_script('jscolor', plugins_url().'/'.$this->core_folder.'js/jscolor/jscolor.js');
        }
    }
    
    public function register_slider()
    {
        if($this->is_correct_page())
        {
            wp_enqueue_script('jquery-ui-slider');
            wp_register_style('jds_events_jquery_ui_css', plugins_url().'/'.$this->core_folder.'css/jquery_ui/custom.css', false, '1.0.0' );
            wp_enqueue_style('jds_events_jquery_ui_css');
        }
    }
    
    public function register_datepicker()
    {
        if($this->is_correct_page())
        {
            wp_enqueue_script('jquery-ui-datepicker');
            wp_register_style('jds_events_jquery_ui_css', plugins_url().'/'.$this->core_folder.'css/jquery_ui/custom.css', false, '1.0.0' );
            wp_enqueue_style('jds_events_jquery_ui_css');
            wp_enqueue_script('datepicker_field_js', plugins_url().'/'.$this->core_folder.'js/jds_core/datepicker_field.js');
            
        }
    }
    
    public function register_repeatable()
    {
        if($this->is_correct_page())
        {
            wp_enqueue_script('jquery');
            wp_enqueue_script('jds_repeatable', plugins_url().'/'.$this->core_folder.'js/jds_core/repeatable.js');
        }
    }

    public function register_switch()
    {
        if($this->is_correct_page())
        {
            wp_enqueue_script('jquery');
            wp_enqueue_script('jds_switch', plugins_url().'/'.$this->core_folder.'js/jds_core/switch.js');
        }
    }
    
    public function register_image()
    {
    	if($this->is_correct_page())
    	{
    		wp_enqueue_script('image_field_js', plugins_url().'/'.$this->core_folder.'js/jds_core/image_field.js');
    	}
    }
    
    public function register_attachment()
    {
    	if($this->is_correct_page())
    	{
    		wp_enqueue_script('attachment_field_js', plugins_url().'/'.$this->core_folder.'js/jds_core/attachment_field.js');
    	}
    }
    
    public function register_css()
    {
        if($this->is_correct_page())
        {
            $version = hash_file('md5', $this->core_path.'css/base.css');
            wp_register_style('jdspf_core_css', plugins_url().'/'.$this->core_folder.'css/base.css', false, $version );
            wp_enqueue_style('jdspf_core_css');
            if (file_exists($this->plugin_path.'css/custom.css'))
            {
                $version = hash_file('md5', $this->plugin_path.'css/custom.css');
                wp_register_style('jdspf_custom_css', plugins_url().'/'.$this->plugin_folder.'css/custom.css', false, $version );
                wp_enqueue_style('jdspf_custom_css');
            }
            else 
            {
                echo "<!-- {$this->plugin_path}css/custom.css does not exist-->";
            }
        }
    }

    public function save_less_variables($array)
    {
        $file = $this->source;
        $text = '';
        foreach($array as $var=>$val)
        {
            // Really wordpress??
            if (function_exists('wp_magic_quotes'))
            {
                $val = stripslashes($val);
                $var = stripslashes($var);
            }

            $text .= "@{$var}: {$val};\n";
        }
        file_put_contents($file, $text);
    }
    
    function process_settings()
    {
        if( isset( $this->filters ) && is_array( $this->filters ) )
        {
            foreach ( $this->filters as $filter_entry )
            {
                $this->register_filter( $filter_entry );
            }
        }
        if ( isset( $this->is_child_field ) && $this->is_child_field === TRUE )
        {
            $this->complete_name = $this->name_prefix . '_' .  $this->name;
        } elseif ( isset( $this->name ) ) {
            $this->complete_name = $this->name;
        }
        if ( isset($this->repeatable) && $this->repeatable )
        {
            add_action( 'admin_enqueue_scripts', array( $this, 'register_repeatable' ) );
        }
        
        if ( isset( $this->require_js ) )
        {
        	$this->register_js( $name = $this->require_js );
        } 
        if ( @$this->load_all_js )
        {
                add_action('admin_enqueue_scripts', array($this, 'register_thickbox'));
                add_action('admin_enqueue_scripts', array($this, 'register_image'));
                add_action('admin_enqueue_scripts', array($this, 'register_attachment'));
                add_action('admin_enqueue_scripts', array($this, 'register_colorpicker'));
                add_action('admin_enqueue_scripts', array($this, 'register_slider'));
                add_action('admin_enqueue_scripts', array($this, 'register_datepicker'));
                add_action('admin_enqueue_scripts', array($this, 'register_repeatable'));
        }
    }
    
    public function add_button_text()
    {
        $text = (@$this->repeat_button_text) ? $this->repeat_button_text : 'insert another '.strtolower($this->title);
        return $text;
    }
    
    public function is_correct_page()
    {
        global $post_type;
        global $pagenow;
        
        // jump through some hoops to get the post type, if applicable, as early as possible
        if ($post_type)
        {
            $post_type_test = $post_type;
        } else {
            if( isset( $_GET['post_type'] ) && $_GET['post_type'] != '')
            {
                $post_type_test = $_GET['post_type'];
            }
            if ( isset( $_POST['post_type'] ) && $_POST['post_type'] != '')
            {
                $post_type_test = $_POST['post_type'];
            }
        }
        // set the page type from the object
        if(isset($this->menu_type))
        {
            $page_type = $this->menu_type;
        } elseif (isset($this->page_menu_type)){
            $page_type = $this->page_menu_type;
        } else {
            $page_type = null;
        }
        
        if(is_admin())
        {
            if(isset($this->post_type) && isset($post_type_test) && $this->post_type == $post_type_test)
            {
                //post type match
                return true;
            }

            if(isset($this->page_slug) && isset($_GET['page']) && $this->page_slug == $_GET['page'] )
            {
                //admin page match
                return true;
            }
            
            if ($pagenow == 'profile.php' && $page_type == 'profile')
            {
                //editing own profile;
                return true;
            }
            if ($pagenow == 'user-edit.php' && $page_type == 'profile')
            {
                //editing anothers profile
                return true;
            }
            if($pagenow =='options.php' && $_POST['options_page'] = $this->page_slug)
            {
                //saving specified options page
                return true;
            }
                 
        }
            return false;
    }
    // the below are field class specific....
    // consider creating a field subclass, which settings and metabox fields then subclass further?
    function wrap_field_template( $content )
    {
        if ( $this::OBJECT_VARIABLE_NAME !== 'field' ) return $content;
        if ( $this->repeatable ) 
        {
            $content = '<div class="repeat_block_holder">' . $content . $this->draw_repeat_button() . '</div>';
        }
        if ( $this->is_child_field !== TRUE ) 
        {
            $content = '<div class="' . $this->draw_field_holder_classes() . 
                       '" id="' . $this->draw_field_holder_id() . '">' . 
                       $this->draw_label() . $content . '</div>';
        } else {
            $content = $this->draw_label() . $content;
        }
        return $content;
    }
    
    function draw_remove_button($key)
    {
        if ($this->repeatable) : ?>    
            &nbsp; <a class="repeatable_remove_button" id="<?=$this->draw_field_container_id($key);?>_remove_button" href="#">remove</a>
        <?php endif;
    }
    
    function draw_repeat_button()
    {
        if ($this->repeatable) {
        $result = '<div class="repeat_button_container"><input id="' . $this->draw_input_id() . '" class="repeat_button" type="button" value="' . $this->add_button_text() . '" /></div>';
        return $result;
        }
    }
    
    function draw_label($classes = '')
    {
        if ( $this->is_child_field === TRUE )
        {
            $classes .= ' child_label';
        }
        if( $this->draw_label === TRUE) {
            return "<label class=\"jds_core_label {$classes} {$this->complete_name}_label \" for=\"{$this->complete_name}\">{$this->title}</label>";
        }
    }
    
    function draw_field_container_classes($index = null, $classes = '')
    {
        if ( $this->repeatable === TRUE )
        {
            $classes .= ' repeat_block ' . $this->complete_name . '_repeat_block';
            if ( @$this->first_class_set !== TRUE)
            {
                $classes .= ' first_block';
                $this->first_class_set = TRUE;
            } else {
                $classes .= ' repeated_block';
            }
        }
        
        if ( is_numeric( $index ) )
        {
            $classes .= ( $index % 2 == 0 ) ? ' even' : ' odd';
        }
        
        if ( $this->is_child_field )
        {
            return "jds_core_field_container {$this->name_prefix}_{$this->name}_container {$this->type}_container is_child {$classes}";
        } else {
            return "jds_core_field_container {$this->name}_container {$this->type}_container {$classes}";
        }
    }
    
    function draw_field_container_id($index = null)
    {
    	if ( ( $this->repeatable || $this->compound ) && $this->is_child_field !== TRUE )
    	{
    		return "{$this->name}_{$index}_container";
    	}
    	elseif ( $this->is_child_field === TRUE )
    	{
    		return $id = $this->get_field_id( $this->parents, $index) . '_container';
    	}
    	else
    	{
    		return "{$this->complete_name}_container";
    	}
        
    }
    
    function draw_input_classes($classes = '')
    {
        return "jds_core_{$this->type} $classes";
    }
    
    function draw_field_holder_classes($classes = '')
    {
        return "jds_core_field_holder {$this->type}_holder $classes";
    }
    
    function draw_field_holder_id( $index = null)
    {
    	if ( $this->is_child_field === TRUE )
    	{
    		return $id = $this->get_field_id( $this->parents, $index) . '_holder';
    	}
    	else
    	{
    		return "{$this->complete_name}_holder";
    	}
    }
    
    function get_field_id( $parents_array,  $index )
    {
    	$name = '';
    	$parent_index = null;
    	if ( is_array($parents_array) ){
    		foreach ( $parents_array as $key=>$obj )
    		{
    			if ( $parent_index === null )
    			{
    				$name .= $obj->name;
    				$parent_index = $key;
    			} else {
    				$name .= '_' . $obj->parent_key . '_' . $obj->name;
    			}
    		}
    		$name .= '_' . $this->parent_key . '_' . $this->name . '_' . $index;
    	}
    	return $name;
    }
    
    function draw_input_id($index = null)
    {
        if ( ( $this->repeatable || $this->compound ) && $this->is_child_field !== TRUE )
        {
            return "{$this->name}_{$index}";
        } 
        elseif ( $this->is_child_field === TRUE ) 
        {
            return $id = $this->get_field_id( $this->parents, $index);
        } 
        else 
        {
            return "{$this->complete_name}";
        }
    }
    
    function get_name_field( $parents_array,  $index )
    {
    	$name = '';
    	$parent_index = null;
    	if ( is_array($parents_array) ){
	    	foreach ( $parents_array as $key=>$obj )
	    	{
	    		if ( $parent_index === null )
	    		{
	    			$name .= $obj->name;
	    			$parent_index = $key;
	    		} else {
					$name .= '[' . $obj->parent_key . ']' . '[' . $obj->name . ']'; 
				}
	    	}
	    	$name .= '[' . $this->parent_key . '][' . $this->name . '][' . $index . ']';
	    }
    	return $name;
    }
    
    function draw_field_name($index = null)
    {
        if ( $this->is_child_field === TRUE )
        {
        	$name = $this->get_name_field( $this->parents, $index);
        	return $name;
        } else {
            return ($this->repeatable || $this->compound ) ? "{$this->name}[{$index}]" : "{$this->name}";
        }
    }
}