<?php
namespace JDSPF\Core\Models\Taxonomies;
class Factory
{
    function __construct($core_folder, $plugin_folder, $root_path)
    {
        $this->core_path = $root_path.$core_folder;
        $this->plugin_path = $root_path.$plugin_folder;
        $this->root_path = $root_path;
        $this->core_folder = $core_folder;
        $this->plugin_folder = $core_folder;
    }
    
    function create_taxonomy($name)
    {
        $new_taxonomy = new Taxonomy($name);
        $new_taxonomy->core_path = $this->core_path;
        $new_taxonomy->plugin_path = $this->plugin_path;
        $new_taxonomy->root_path = $this->root_path;
        $new_taxonomy->core_folder = $this->core_folder;
        $new_taxonomy->plugin_folder = $this->plugin_folder;
        return $new_taxonomy;
    }
}