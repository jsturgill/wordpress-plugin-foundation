<?php
// Set up one-only metabox pulldown option...
// Basically, a view for the taxonomy metabox
// http://shibashake.com/wordpress-theme/wordpress-custom-taxonomy-input-panels

// Add additional fields to the taxonomy
// http://shibashake.com/wordpress-theme/expand-the-edit-category-admin-panel


namespace JDSPF\Core\Models\Taxonomies;
use JDSPF\Core\Models\Entity as Base;
class Taxonomy extends Base
{
    public $type;
    
    public $setting_blacklist = array(
        
    );
    
    // template information
    public $template;
    const OBJECT_VARIABLE_NAME = 'taxonomy';
    const TEMPLATE_PATH = 'taxonomies/';
    
    // defaults
    const DEFAULT_TYPE = 'taxonomy';
    const DEFAULT_POST_TYPES = 'post'; //can be an array (multiple) or string (single)
    const DEFAULT_HIERARCHICAL_FLAG = true;
    const DEFAULT_QUERY_FLAG = true;
    const DEFAULT_REWRITE_FLAG = true;
    const DEFAULT_PUBLIC_FLAG = true;
    const DEFAULT_SHOW_IN_NAV_MENUS_FLAG = true;
    const DEFAULT_SHOW_UI_FLAG = true;
    const DEFAULT_SHOW_ADMIN_COLUMN_FLAG = true;
    const DEFAULT_SHOW_TAGCLOUD_FLAG = true;
    const DEFAULT_METABOX_TEMPLATE = null;
    function __construct($name)
    {
        $this->type = $this::DEFAULT_TYPE;
        $this->template = $this->type;
        $this->name = $name;
        $this->label = $name;
        $this->singular = $name;
        $this->plural = $name;
        $this->hierarchical = $this::DEFAULT_HIERARCHICAL_FLAG;
        $this->query_var = $this::DEFAULT_QUERY_FLAG;
        $this->rewrite = $this::DEFAULT_REWRITE_FLAG;
        $this->post_types = $this::DEFAULT_POST_TYPES;
        $this->public = $this::DEFAULT_PUBLIC_FLAG;
        $this->show_in_nav_menus = $this::DEFAULT_SHOW_IN_NAV_MENUS_FLAG;
        $this->show_ui = $this::DEFAULT_SHOW_UI_FLAG;
        $this->show_admin_column = $this::DEFAULT_SHOW_ADMIN_COLUMN_FLAG;
        $this->metabox_template = $this::DEFAULT_METABOX_TEMPLATE;
        add_action( 'init', array($this, 'register'));  
    }
    
    function register()
    {
        register_taxonomy(   
            $this->name,   
            $this->post_types,
            array(   
                'hierarchical' => $this->hierarchical,   
                'label' => $this->label,   
                'query_var' => $this->query_var,   
                'rewrite' => $this->rewrite,
                'public' => $this->public,
                'show_in_nav_menus' => $this->show_in_nav_menus,
                'show_ui' => $this->show_ui,
                'show_admin_column'=>$this->show_admin_column,
                'labels' => array(
                    'name' => $this->plural,
                    'singular_name' => $this->singular,
                    'plural_name' => $this->plural,
                    'search_items' => "Search $this->plural",
                    'popular_items' => "Popular $this->plural",
                    'all_items' => "All $this->plural",
                    'parent_item' => "Parent $this->singular",
                    'parent_item_colon' => "Parent $this->singular:",
                    'edit_item' => "Edit $this->singular",
                    'update_item' => "Update $this->singular",
                    'add_new_item' => "Add a new ".strtolower($this->singular),
                    'new_item_name' => "New $this->singular name",
                    'separate_items_with_commas' => "Separate ".strtolower($this->plural)." with commas",
                    'add_or_remove_items' => "Add or remove $this->plural",
                    'choose_from_most_used' => "Choose from the most used ".strtolower($this->plural),
                    'menu_name' => $this->label
                )
            )
        );
    }
 
    function set_metabox_template()
    {
        if (is_admin() && $this->template !== 'taxonomy')
        {
            add_action('add_meta_boxes', array($this, 'register_custom_meta_box'));
            $this->show_ui = false;
        }
    }
    
    function process_settings()
    {
        if ($this->template)
        {
            $this->set_metabox_template();
        }
    }
 
    function register_custom_meta_box()
    {
        global $post_type;
        if ($post_type == $this->post_types || (is_array($this->post_types) && in_array($post_type, $this->post_types)))
        {
            add_meta_box($this->name.'_edit_box', $this->singular.' box', array($this, 'draw'), $post_type, 'side', 'core');
        }
    }
}