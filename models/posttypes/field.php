<?php
namespace JDSPF\Core\Models\PostTypes;
use JDSPF\Core\Functions;
use JDSPF\Core\Models\Entity as Base;
class Field extends Base
{
    public $name;
    public $type;
    public $setting_blacklist = array(
        'name'=>null,
    );
    public $html = '[content]';
    public $required_scripts;
    public $child_fields;
    public $parents;
    public $name_prefix;
    public $is_child_field;
    public $repeatable;
    public $compound;
    public $select_label;
    public $default_value;
    const DEFAULT_TYPE = 'textbox';
    
    // template information
    public $template;
    const OBJECT_VARIABLE_NAME = 'field';
    const TEMPLATE_PATH = 'meta_boxes/fields/';
    
    //defaults
    const DRAW_LABEL_DEFAULT = true;
    
    function __construct($name, $type)
    {
        $this->name = $this->string_to_slug($name);
        $this->type = $type;
        $this->template = $type;
        $this->title = $name;
        $this->draw_label = $this::DRAW_LABEL_DEFAULT;
        
        switch ($type)
        {
            case 'datepicker':
                add_action('admin_enqueue_scripts', array($this, 'register_datepicker'));
            break;
            case 'slider':
                add_action('admin_enqueue_scripts', array($this, 'register_slider'));
            break;
            case 'colorpicker':
                add_action('admin_enqueue_scripts', array($this, 'register_colorpicker'));
            break;
            case 'less_variables':
                add_action('admin_enqueue_scripts', array($this, 'register_colorpicker'));
            break;
            case 'attachment':
                add_action('admin_enqueue_scripts', array($this, 'register_thickbox'));
                add_action('admin_enqueue_scripts', array($this, 'register_attachment'));
            break;
            case 'image':
                add_action('admin_enqueue_scripts', array($this, 'register_thickbox'));
                add_action('admin_enqueue_scripts', array($this, 'register_image'));
            break;
            case 'switch':
                add_action('admin_enqueue_scripts', array($this, 'register_switch'));
            break;
        }
    }
    
    public function draw($parent_key = NULL)
    {
        // TODO: refactor so that fields always use array (false as third value)
        // so that the same loop will be used in the damn fields views!
        if ($this->is_child_field !== TRUE ) $this->value();
        parent::draw( $parent_key );
    }
    
    public function value()
    {
        global $post;
        if ($post && ($this->repeatable || $this->compound))
        {
            $this->value = get_post_meta($post->ID, $this->name, false); // always returns an array
            if ( $this->compound ) 
            {
                foreach($this->value as $key=>$value)
                {
                    if ( empty( $value) ) {
                        unset( $this->value[$key] );
                    } else {
                        $this->value[$key] = maybe_unserialize( $value );
                    }
                }
                $this->value = \JDSPF\Core\Functions\sort_field_values( $this->value );
            } elseif (!empty( $this->value ) )  {
                $val = array_shift( $this->value );
                $this->value = maybe_unserialize( $val );
            }
            $this->value = ( empty( $this->value ) ) ? null : $this->value;
            
        } elseif ( $post ) {
            $this->value = get_post_meta( $post->ID, $this->name, true );
        }
        return $this->value;
    }
    
    public function process_settings()
    {
      if ( is_array( $this->required_scripts ) ) 
      {
        foreach( $this->required_scripts as $script )
        {
          switch ($script)
          {
            case 'thickbox':
              add_action('admin_enqueue_scripts', array($this, 'register_thickbox'));
            break;
          }
        }
      }
      parent::process_settings();
    }
    
    public function save($post_id)
    {
        // TODO: need to add validation
        if($this->type=='less_variables')
        {
            $this->save_less_variables();
            return;
        }

        if($this->repeatable || $this->compound)
        {
            delete_post_meta( $post_id, $this->name );
            $values = ( is_array( @$_POST[$this->name] ) ) ? @$_POST[$this->name] : array( 0=>@$_POST[$this->name] );
            if ( $this->compound ) 
            { 
                foreach( $values as $field_key=>$field_value )
                {
                    if ( is_array( $field_value ) )
                    {
                	  	$field_value['_sort_order'] = $field_key;
                    }
                    $field_value = Functions\trim_array( $field_value );
                    add_post_meta($post_id, $this->name, $field_value);
                }
            } else {
                add_post_meta( $post_id, $this->name, $values );
            }
        } else {
            $old = get_post_meta($post_id, $this->name, true);   
            $new = trim( @$_POST[$this->name] );
            if ($new && $new != $old) {   
                update_post_meta($post_id, $this->name, $new);
            } elseif ('' == $new && $old) {
                delete_post_meta($post_id, $this->name, $old);   
            }
        }
    }
}