<?php
namespace JDSPF\Core\Models\PostTypes;
use JDSPF\Core\Functions;
use JDSPF\Core\Models\Entity as Base;
class Type extends Base
{
    public $slug;
    public $singular;
    public $plural;
    
    public $crud_roles = array();
    public $editor_roles = array();
    
    public $setting_blacklist = array(
        'fields'=>null,
        'meta_boxes'=>null,
    );
    
    const DEFAULT_PUBLIC = True;
    const DEFAULT_PUBLICLY_QUERYABLE = True;
    const DEFAULT_SHOW_UI = True;
    const DEFAULT_SHOW_IN_MENU = True;
    const DEFAULT_QUERY_VAR = True;
    const DEFAULT_REWRITE = True;
    static $DEFAULT_CAPABILITY_TYPE = array('post', 'posts');
    const DEFAULT_HAS_ARCHIVE = True;
    const DEFAULT_HIERARCHICAL = True;
    const DEFAULT_MENU_POSITION = null;
    static $DEFAULT_SUPPORTS = array( 'title', 'editor', 'page-attributes' ); //constants can't be arrays; arrays are always mutable.  Sadface.
    function __construct($slug)
    {
        // important items first
        $this->slug = $slug;
        $this->post_type = $slug;
        $this->singular = $slug;
        $this->plural = $slug;
        $this->capability_type = $slug;
        
        // wp actions
        add_action('init', array($this, 'register'));
        add_action('admin_enqueue_scripts', array($this, 'register_css'));
        
        // set up default values
        $this->public = Type::DEFAULT_PUBLIC;
        $this->publicly_queryable = Type::DEFAULT_PUBLICLY_QUERYABLE;
        $this->show_ui = Type::DEFAULT_SHOW_UI;
        $this->show_in_menu = Type::DEFAULT_SHOW_IN_MENU;
        $this->query_var = Type::DEFAULT_QUERY_VAR;
        $this->rewrite = Type::DEFAULT_REWRITE;
        //$this->capability_type = Type::$DEFAULT_CAPABILITY_TYPE; //go with slug...
        $this->has_archive = Type::DEFAULT_HAS_ARCHIVE;
        $this->hierarchical = Type::DEFAULT_HIERARCHICAL;
        $this->menu_position = Type::DEFAULT_MENU_POSITION;
        $this->supports = Type::$DEFAULT_SUPPORTS;
        $this->taxonomies = array();
        $this->show_in_nav_menus = true;

    }
    
    public function set_title_field_prompt()
    {
        if($this->is_correct_page())
        {
            add_filter('enter_title_here', array($this, 'draw_title_field'));
        }
    }
    
    public function process_settings()
    {
        if ( isset( $this->title_field_prompt ) ) $this->set_title_field_prompt();
        if ( isset( $this->crud_roles ) && !is_array($this->crud_roles)) $this->crud_roles = array($this->crud_roles);
        if ( isset( $this->supports ) && !is_array($this->supports)) $this->supports = array($this->supports);
        if ( isset( $this->editor_roles ) && !is_array($this->editor_roles)) $this->editor_roles = array($this->editor_roles);
        if ( isset( $this->remove_publish_box ) ) $this->remove_meta_box('submitdiv', 'side');
        if ( isset( $this->hide_other_crud_users_posts ) ) $this->register_crud_hiding();       
        parent::process_settings();
    }
    
    function register_crud_hiding()
    {
        $object = $this;
        $user_callback = function ($wp_query) use ($object)
        {
            global $current_user;
            $plural_cap = (is_array($object->capability_type)) ? $object->capability_type[1] : $object->capability_type.'s';
            if ( is_admin() && $object->is_correct_page() && !current_user_can('edit_others_'.$plural_cap) ) 
            {
                $wp_query->set( 'author', $current_user->ID );
                add_filter("views_edit-{$plural_cap}", function($views){
                    $views = array();
                    return $views;});
            }
        };
        add_action('pre_get_posts', $user_callback);
    }
    
    function remove_meta_box($div, $context)
    {
        $type = $this->slug;
        $callback = function() use($div, $context, $type)
        {
            remove_meta_box( $div, $type, $context );
        };
        add_action( 'admin_menu', $callback );
    }
    

    
    public function draw_title_field()
    {
        $screen = get_current_screen();
        $prompt = 'Enter title here';
        if  ( $this->slug == $screen->post_type ) 
        {
            $prompt = $this->title_field_prompt;
        }
        return $prompt;
    }
    
    public function register()
    {
        $labels = array(
            'name' => $this->plural,
            'singular_name' => $this->singular,
            'add_new' => 'Add new ' . $this->singular,
            'add_new_item' => 'Add new '.$this->singular,
            'edit_item' => 'Edit '.$this->singular,
            'new_item' => 'New '.$this->singular,
            'all_items' => 'All '.$this->plural,
            'view_item' => 'View '.$this->singular,
            'search_items' => 'Search '.$this->plural,
            'not_found' =>  'No '.$this->plural.' found',
            'not_found_in_trash' => 'No '.$this->plural.' found in trash',
            'parent_item_colon' => '',
            'menu_name' => $this->plural,
            );
        $labels = (isset($this->labels)) ? array_merge($labels, $this->labels) : $labels;
        
        $args = array(
            'labels' => $labels,
            'public' => $this->public,
            'publicly_queryable' => $this->publicly_queryable,
            'show_ui' => $this->show_ui, 
            'show_in_menu' => $this->show_in_menu, 
            'query_var' => $this->query_var,
            'rewrite' => $this->rewrite,
            'capability_type' => $this->capability_type,
            'map_meta_cap' => true,
            'has_archive' => $this->has_archive, 
            'hierarchical' => $this->hierarchical,
            'menu_position' => $this->menu_position,
            'supports' => $this->supports,
            'taxonomies' => $this->taxonomies,
            'show_in_nav_menus' => $this->show_in_nav_menus,
            );
        register_post_type($this->slug, $args);
        $this->register_roles(); // allow users to actually use the damn thing...
    }
    public function register_roles()
    {
        // add a check...
        // roles are stored in DB, so no need to do this every time.
        // fire once and forget it.  How best to do so?
        global $wp_roles;
        if (isset($wp_roles)) :
        $roles = $wp_roles->get_names(); //array($role_slug=>$role_display, ...)
        if (is_array($this->capability_type))
        {
            $sc = strtolower($this->capability_type[0]);  // singular
            $pc = strtolower($this->capability_type[1]); // plural
        }
        else
        {
            $sc = strtolower($this->capability_type);
            $pc = strtolower($this->capability_type).'s';
        }
        //functions\pre_print_r(get_post_types(null, 'objects'));
        //$user = wp_get_current_user();
        //functions\pre_print_r($user);
        $crud_callback = function($wp_roles, $role) use ($sc, $pc)
        {
            //$wp_roles->add_cap($role, "edit_posts");
            $perms = array("edit_$sc",
                    "read_$sc",
                    "delete_$sc",
                    "edit_$pc",
                    "publish_$pc",
                    "read_private_$pc",
                    "read",
                    "delete_$pc",
                    "delete_private_$pc",
                    "delete_published_$pc",
                    "edit_private_$pc",
                    "edit_published_$pc");
            foreach($perms as $perm)
            {
                $wp_roles->add_cap($role, $perm);
            }
            
        };
        
        $editor_callback = function($wp_roles, $role) use ($sc, $pc)
        {
            $perms = array("edit_$sc",
                    "read_$sc",
                    "delete_$sc",
                    "edit_$pc",
                    "edit_others_$pc",
                    "publish_$pc",
                    "read_private_$pc",
                    "read",
                    "delete_$pc",
                    "delete_private_$pc",
                    "delete_published_$pc",
                    "delete_others_$pc", 
                    "edit_private_$pc",
                    "edit_published_$pc");
            foreach($perms as $perm)
            {
                $wp_roles->add_cap($role, $perm);
            }
        };
        
        foreach ($this->crud_roles as $role)
        {
            if (array_key_exists($role, $roles)) // If the role is valid
            {
                $crud_callback($wp_roles, $role);
            }
        }
        foreach ($this->editor_roles as $role)
        {
            if (array_key_exists($role, $roles)) // If the role is valid
            {
                $editor_callback($wp_roles, $role);
            }
        }
    endif;
    }
}