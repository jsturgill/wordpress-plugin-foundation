<?php
// http://wp.tutsplus.com/tutorials/reusable-custom-meta-boxes-part-1-intro-and-basic-fields/
// aside from other types in article,
// have (repeatable and singular) fileboxes!
// http://wp.tutsplus.com/tutorials/attaching-files-to-your-posts-using-wordpress-custom-meta-boxes-part-1/

namespace JDSPF\Core\Models\PostTypes;
use JDSPF\Core\Functions;
use JDSPF\Core\Models\Entity as Base;
class MetaBox extends Base
{
    public $id;
    public $post_type;
    public $fields = array();
    public $parent_capability_type;
    public $required_template;
    public $required_value;
    public $setting_blacklist = array(
        'fields'=>null,
    );
    
    // template information
    public $template;
    const OBJECT_VARIABLE_NAME = 'metabox';
    const TEMPLATE_PATH = 'meta_boxes/';
    const DEFAULT_TEMPLATE = 'default';
    const DEFAULT_CONTEXT = 'normal';
    
    function __construct($id, $post_type)
    {
        $this->id = $id;
        $this->nonce_id = $id.'_nonce';
        $this->post_type = $post_type;
        $this->title = $id;
        $this->template = $this::DEFAULT_TEMPLATE;
        $this->context = $this::DEFAULT_CONTEXT;
        add_action('add_meta_boxes', array($this, 'register'));
        add_action('save_post', array($this, 'save_fields')); // the metabox controls when & if the fields save.
        add_action('admin_enqueue_scripts', array($this, 'register_css'));
    }

    function register()
    {
        add_meta_box( $this->id, $this->title, array($this, 'draw'), $this->post_type, $this->context, 'high');
    }
    
    function get_nonce()
    {
        return wp_create_nonce(basename(__FILE__));
    }
    
    function verify_nonce()
    {
        if( isset( $_POST[$this->nonce_id] ) )
        {
            return wp_verify_nonce($_POST[$this->nonce_id], basename(__FILE__));
        } else {
            return false;
        }
    }
        
    function draw_fields()
    {
        foreach ($this->fields as $field)
        {
            if ( $field->is_child_field !== TRUE )
            {
                $field->draw();
            }
        }
        if ($this->required_template)
        {
            Functions\render_js_snippet('required_post_template', array(
                'filename'=>$this->required_template,
                'target'=>$this->id)); 
        }
        if ( is_array( $this->required_value ) )
        {
            Functions\render_js_snippet('required_value', array(
                'metabox'=>$this->id,
                'check_target'=>$this->required_value['check_target'],
                'value'=>$this->required_value['value'],
                )); 
        }
    }
    
    function draw_nonce()
    {
        echo "<input type='hidden' name='$this->nonce_id' value='{$this->get_nonce()}' />";
    }
    
    function save_fields($post_id)
    {
        $capability_check = (is_array($this->parent_capability_type)) ? $this->parent_capability_type[0] : $this->parent_capability_type;
        global $post;
        if (isset( $_POST['post_type'] ) && $_POST['post_type'] != $this->post_type) return $post_id;
        if (!current_user_can( 'edit_post', $post_id )) return $post_id;
        if (!$this->verify_nonce()) return $post_id;
        foreach ($this->fields as $field)
        {
                $field->save($post_id);
        }
        $path = $this->plugin_path.'save_actions/meta_boxes/'.@$this->save_action.'.php';
        if (@$this->save_action && file_exists($path))
        {
            extract(array('metabox'=>$this));
            include($path);
        }
    }
}