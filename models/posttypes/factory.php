<?php
namespace JDSPF\Core\Models\PostTypes;
class Factory
{
    function __construct($core_folder, $plugin_folder, $root_path)
    {
        $core_path = $root_path.$core_folder;
        $plugin_path = $root_path.$plugin_folder;
        $this->core_path = $core_path;
        $this->plugin_path = $plugin_path;
        $this->root_path = $root_path;
        $this->core_folder = $core_folder;
        $this->plugin_folder = $plugin_folder;
    }
	public function create_type($name)
	{
		$new_type = new Type($name);
        $new_type->core_path = $this->core_path;
        $new_type->plugin_path = $this->plugin_path;
        $new_type->root_path = $this->root_path;
        $new_type->core_folder = $this->core_folder;
        $new_type->plugin_folder = $this->plugin_folder;
		return $new_type;
	}
    
    public function create_meta_box($type_object_or_slug, $id)
    {
        if (is_object($type_object_or_slug))
        {
            $new_meta_box = new MetaBox($id, $type_object_or_slug->slug);
            $new_meta_box->parent_capability_type = $type_object_or_slug->capability_type;
            $type_object_or_slug->meta_boxes[] = $new_meta_box;
        }
        else
        {
            $new_meta_box = new MetaBox($id, $type_object_or_slug);
            $new_meta_box->parent_capability_type = $type_object_or_slug;
        }
        $new_meta_box->core_path = $this->core_path;
        $new_meta_box->plugin_path = $this->plugin_path;
        $new_meta_box->root_path = $this->root_path;
        $new_meta_box->core_folder = $this->core_folder;
        $new_meta_box->plugin_folder = $this->plugin_folder;
        return $new_meta_box;
    }
    
    public function create_field($meta_box_object, $name, $type=Field::DEFAULT_TYPE)
    {
        $new_field = new Field($name, $type);
        $new_field->post_type = $meta_box_object->post_type;
        $meta_box_object->fields[] = $new_field;
        $new_field->core_path = $this->core_path;
        $new_field->plugin_path = $this->plugin_path;
        $new_field->root_path = $this->root_path;
        $new_field->core_folder = $this->core_folder;
        $new_field->plugin_folder = $this->plugin_folder;
        return $new_field;
    }
    
    public function create_child_field( $meta_box_object, $name, $type=Field::DEFAULT_TYPE, $parent_field )
    {
        $new_field = $this->create_field( $meta_box_object, $name, $type );
        $parent_field->children[] = $new_field;
        if ( is_array( $parent_field->parents ) )
        {
        	$new_field->parents = $parent_field->parents;
        }
        $new_field->parents[] =& $parent_field;
        if ( $parent_field->name_prefix )
        {
            $new_field->name_prefix = $parent_field->name_prefix . '_' . $parent_field->name;
        } else {
            $new_field->name_prefix = $parent_field->name;
        }
        $new_field->is_child_field = TRUE;
        $parent_field->compound = true;
        return $new_field;
    }
}