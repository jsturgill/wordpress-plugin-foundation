<?php
namespace JDSPF\Core\Models;
use JDSPF\Core\Functions;
use JDSPF\Core\Models\Core as Core;
class Plugin
{
    public static $foundation_path;
    public static $wp_plugin_path;
    public static $foundation_folder;
    public $plugin_path;
    public $plugin_folder;
    public $plugin_scripts = array();
    public $plugin_scripts_action_added = false;
    public static $core_scripts = array();
    public static $core_scripts_action_added = false;
    public static $js_namespace;
    public $plugin_css_action_added = false;
    public static $plugin_css = array();
    
    function __construct( $plugin_folder )
    {
        $core = Core::get_core_object();
        self::$foundation_path = $core::$foundation_path;
        self::$foundation_folder = $core::$foundation_folder;
        self::$wp_plugin_path = $core::$wp_plugin_path;
        $sep = DIRECTORY_SEPARATOR;
        $plugin_folder = rtrim( $plugin_folder, '/\\');
        $this->plugin_path = self::$wp_plugin_path . $plugin_folder . $sep;
        $this->plugin_folder = $plugin_folder . $sep;
        self::$js_namespace = $plugin_folder;
        $core->register_plugin( $this );
    }
    
    // the root of the path is the particular plugin's home folder.
    public function get_file_url( $path )
    {
        $url = plugins_url( $this->plugin_folder . $path , $this->plugin_path );
        return $url;
    }

    public function get_bootstrap_json()
    {
        return '';
    }
    
    public function render( $template, $variables = array() )
    {
        $path = $this->plugin_path . 'views/' . $template . '.php';
        if ( file_exists( $path ) )
        {
            if (is_array( $variables ) ) extract( $variables );
            include( $path );
        }
    }
    
    public function get_content( $template, $variables = array() )
    {
        ob_start();
        $this->render( $template, $variables );
        $content = ob_get_clean();
        return $content;
    }
    
    public function require_script( $script )
    {
        Core::require_plugin_script( $this->plugin_folder, $script );
    }

    public function require_library_script( $script )
    {
        Core::require_plugin_library_script( $this->plugin_folder, $script );
    }

    public function require_php_library( $script )
    {
        Core::require_plugin_php_library( $this->plugin_folder, $script );
    }
    
    public function require_css( $css )
    {
        $path = $this->plugin_folder . 'css/' . $css . '.css';
        if ( file_exists( $this::$wp_plugin_path . $path ) ) {
            $url = plugins_url( $path );
            $this->plugin_css[$this::$wp_plugin_path . $path] = $url;
            if ( $this->plugin_css_action_added !== true )
            {
                add_action( 'wp_head', array( $this, 'render_css' ) );
                $this->plugin_css_action_added = true;
            }
        } 
        
    }
    
    public function render_css()
    {
        $this->plugin_css = array_unique( $this->plugin_css );
        foreach ( $this->plugin_css as $path=>$url) {
            $version = hash_file( 'crc32b', $path );
            $url = esc_attr( $url );
            echo "<link type='text/css' rel='stylesheet' media='all' href='{$url}?v={$version}' />";
        }
    }
    
    public function require_core_library_script( $script )
    {
        Core::require_core_library_script( $script );
    }
    
    public function require_core_script( $script )
    {
        Core::require_core_script( $script );
    }
}