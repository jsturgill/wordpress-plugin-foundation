<?php
namespace JDSPF\Core\Models;
use JDSPF\Core\Functions;
class Core
{
    public static $foundation_path;
    public static $wp_plugin_path;
    public static $foundation_folder;
    public static $core_instance;
    public static $plugins = array();
    public static $initialized = false;
    public static $js_global_object_name = 'JDSPF'; 
    public static $core_lib_scripts = array();
    public static $core_scripts = array();
    public static $plugin_lib_scripts = array();
    public static $plugin_scripts = array();
    public static $render_scripts_action_added = false;
    
    private function __construct( )
    {
    }
    
    public static function get_core_object()
    {
        if ( !isset( $core_instance ) ) {
            self::$core_instance = new Core;
        }
        return self::$core_instance;
    }
    
    public static function register_plugin( $plugin )
    {
        self::$plugins[] = $plugin;
        if ( self::$initialized !== false )
        {
            self::bootstrap( $plugin );
        } 
    }
    
    public static function bootstrap( $plugin )
    {
        require_once( self::$foundation_path . 'functions.php');
        Functions\bootstrap( $plugin->plugin_folder, self::$foundation_folder, self::$wp_plugin_path );
        self::register_plugin_js_namespace( $plugin );
    }
    
    public static function initialize( $foundation_path, $wp_plugin_path, $foundation_folder )
    {
        self::$foundation_path = $foundation_path;
        self::$wp_plugin_path = $wp_plugin_path;
        self::$foundation_folder = $foundation_folder;
        self::$initialized = true;
        self::register_global_js_object();
        self::require_core_library_script( 'underscore/underscore-min' );
        self::require_core_library_script( 'json2js/json2' );
        self::require_core_library_script( 'backbone/backbone-min' );
        self::compile_less( self::$foundation_path . 'css/less/main.less', self::$foundation_path . 'css/compiled.css' );
        if ( is_admin() )
        {
            $callback = function() {
                wp_enqueue_script( 'jquery-ui-sortable' );
                wp_enqueue_script( 'jquery-effects-highlight' );
            };
            add_action( 'admin_enqueue_scripts', $callback );
        }
        foreach( self::$plugins as $plugin )
        {
            self::bootstrap( $plugin );
        }
        
    }

    public static function set_plugin_load_order()
    {
        $path = self::$foundation_folder.'index.php';
        if ( $plugins = get_option( 'active_plugins' ) ) {
            if ( $key = array_search( $path, $plugins ) ) {
                array_splice( $plugins, $key, 1 );
                array_unshift( $plugins, $path );
                update_option( 'active_plugins', $plugins );
            }
        }
    }
    
    public static function require_core_library_script( $rel_path )
    {
        $abs_path = self::$foundation_path . 'lib/scripts/' . $rel_path . '.js';
        $url_path = self::$foundation_folder . 'lib/scripts/' . $rel_path . '.js';
        if ( file_exists( $abs_path ) )
        {
            $url = plugins_url( $url_path );
            self::$core_lib_scripts[$abs_path] = $url;
            if ( self::$render_scripts_action_added !== true )
            {
                add_action( 'wp_footer', '\JDSPF\Core\Models\Core::render_scripts' );
                self::$render_scripts_action_added = true;
            }
        }
    }
    
    public static function require_core_script( $rel_path )
    {
        $abs_path = self::$foundation_path . 'js/jds_core/' . $rel_path . '.js';
        $url_path = self::$foundation_folder . 'js/jds_core/' . $rel_path . '.js';
        if ( file_exists( $abs_path ) )
        {
            $url = plugins_url( $url_path );
            self::$core_scripts[$abs_path] = $url;
            if ( self::$render_scripts_action_added !== true )
            {
                add_action( 'wp_footer', '\JDSPF\Core\Models\Core::render_scripts' );
                self::$render_scripts_action_added = true;
            }
        } else {
        }
    }

    public static function require_plugin_library_script( $rel_path )
    {
        $abs_path = self::$plugin_path . 'lib/scripts/' . $rel_path . '.js';
        $url_path = self::$plugin_folder . 'lib/scripts/' . $rel_path . '.js';
        if ( file_exists( $abs_path ) )
        {
            $url = plugins_url( $url_path );
            self::$plugin_lib_scripts[$abs_path] = $url;
            if ( self::$render_scripts_action_added !== true )
            {
                add_action( 'wp_footer', '\JDSPF\Core\Models\Core::render_scripts' );
                self::$render_scripts_action_added = true;
            }
        }
    }
    
    public static function require_plugin_script( $plugin_folder, $rel_path )
    {
        $abs_path = self::$wp_plugin_path . $plugin_folder . 'js/' . $rel_path . '.js';
        $url_path = $plugin_folder . 'js/' . $rel_path . '.js';
        if ( file_exists( $abs_path ) )
        {
            $url = plugins_url( $url_path );
            self::$plugin_scripts[$abs_path] = $url;
            if ( self::$render_scripts_action_added !== true )
            {
                add_action( 'wp_footer', '\JDSPF\Core\Models\Core::render_scripts' );
                self::$render_scripts_action_added = true;
            }
        }
    }
    
    public static function require_core_php_library( $rel_path )
    {
        $abs_path = self::$foundation_path . 'lib/' . $rel_path . '.php';
        if ( file_exists( $abs_path ) ) 
        {
            require_once( $abs_path );
        }
    }
    
    public static function require_plugin_php_library( $plugin_folder, $rel_path )
    {
        $abs_path = self::$wp_plugin_path . $plugin_folder . 'lib/' . $rel_path . '.php';
        if ( file_exists( $abs_path ) ) 
        {
            require_once( $abs_path );
        }
    }

    // absolute paths
    public static function compile_less( $source, $destination )
    {
        self::require_core_php_library( 'lessphp/lessc.inc' );
        $less = new \lessc;
        if ( file_exists( $source ) ) 
        {
            try {
                $less->checkedCompile( $source, $destination );
            } catch ( \exception $e ) {
                error_log( "fatal error: " . $e->getMessage() ); 
            }
        }
    }

    public static function render_scripts()
    {
        // set an explicit order: core_lib, core, plugin_lib, plugin
        self::$core_lib_scripts = array_unique( self::$core_lib_scripts );
        self::$core_scripts = array_unique( self::$core_scripts );
        self::$plugin_scripts = array_unique( self::$plugin_scripts );
        $script_queue[] = self::$core_lib_scripts;
        $script_queue[] = self::$core_scripts;
        $script_queue[] = self::$plugin_lib_scripts;
        $script_queue[] = self::$plugin_scripts;
        foreach( $script_queue as $script_array )
        {
            if ( !empty( $script_array ) )
            {
                foreach ( $script_array as $path=>$url )
                {
                    $url = esc_attr($url);
                    echo "<script type='text/javascript' src='{$url}'></script>";
                    echo "<script type='text/javascript'>JDSPF.loaded_scripts.push('$url')</script>";
                }
            }
        }
    }
    
    public static function register_plugin_js_namespace( $plugin )
    {
        
        $name = self::$js_global_object_name . '.' . $plugin::$js_namespace;
        $callback = function() use ( $name, $plugin )
        {
            $json = $plugin->get_bootstrap_json();
            echo "<script type='text/javascript'>$name = { $json };</script>";
        };
        
        if ( is_admin() )
        {
            add_action( 'admin_head', $callback );
        } else {
            add_action( 'wp_head', $callback );
        }
    }
    
    public static function register_global_js_object()
    {
        $name = self::$js_global_object_name;
        $url_path = self::$foundation_folder . 'js/';
        $url = esc_attr( plugins_url( $url_path ) );
        $callback = function() use ( $name, $url )
        {
            echo "<script type='text/javascript'>$name = {
                scripts_url: '$url'
            };</script>";
        };
        if ( is_admin() )
        {
            add_action( 'admin_head', $callback );
        } else {
            add_action( 'wp_head', $callback );
        }
    }
}