<?php
namespace JDSPF\Core\Models\AdminPages;
use JDSPF\Core\Functions;
use JDSPF\Core\Models\Entity as Base;
Class Field extends Base
{
	public $validator;
	public $group;
	public $name; // must be unique; controls data storage and retrieval
	public $type;
	public $id; // must be unique; controls HTML
	public $title; // appears to the left of the option; defaults to $name
	public $html = '[content]'; // replace with string 
	public $repeatable;
    public $compound;
    public $required_scripts;
    public $is_child_field;
    public $filters;
    public $child_fields;
    public $parents;
    public $name_prefix;
    public $is_user_option;
    public $is_user_admin_option;
    public $repeat_button_text;
    public $default_value;

	const DEFAULT_TYPE = 'textbox';
    //const DEFAULT_VALIDATOR = null;
    
    // template information
    public $template;
    const OBJECT_VARIABLE_NAME = 'field';
    const TEMPLATE_PATH = 'pages/fields/';
    
    //defaults
    const DRAW_LABEL_DEFAULT = false;
    
	function __construct($group, $name) 
	{
		$this->group = $group;
		$this->name = $this->clean_name($name);
		$this->id = "id_{$this->clean_name($name)}";
		$this->title = $name;
		$this->validator = array($this, 'sanitize_string');
		$this->type = $this::DEFAULT_TYPE;
        $this->draw_label = $this::DRAW_LABEL_DEFAULT;
		add_action('admin_init', array(&$this, 'register'));
	}
    
	private function clean_name($string)
	{
		$string = preg_replace('/[\W]+/i', "_", $string); // convert everything that isn't wordy to _
		$string = strtolower(str_replace('__', '_', $string)); // ensure no more than one _ in a row
		return $string;
	}
	
	function register() 
	{
            // manually validated if repeatable
            // (see save function)
			if($this->repeatable || $this->compound) 
            {   $validator = function($string) {
                    return $string;
                };
            } else {
                $validator = ($this->validator === NULL) ? array($this, 'sanitize_string') : $this->validator;
            }
			register_setting($this->group, $this->name, $validator);
	}
	
    public function save()
    {
        if ($this->type=='less_variables') 
        {
            if($this->verify_nonce())
            {
                $this->save_less_variables((array)$_POST[$this->name]);
            }
        } elseif ($this->verify_nonce() && $this->is_child_field !== TRUE ) {
            if ($this->repeatable || $this->compound)
            {
                $value = $_POST[$this->name];
                if ( is_array( $value ) )
                {
                    $value = \JDSPF\Core\Functions\trim_array( $value );
                    foreach( $value as $key=>$val ):
                        if( empty( $val ) && $val !== '0' && $val !== 0 ) unset( $value[$key] );
                    endforeach;
                } else {
                    $value = array( 0=>trim( $value ) );
                }
                if ( empty( $value ) ) {
                    delete_option( $this->name );
                    $_POST[$this->name] = '';
                } else {
                    $_POST[$this->name] = serialize( $value );
                }
            }
        }
    }
    
    public function verify_nonce()
    {
        if ($this->page_menu_type == 'profile')
        {
            if (wp_verify_nonce($_POST['_wpnonce'], 'update-user_'.(int)$_POST['user_id']))
            {
                return true;
            } else {
                return false;
            }
        } else {
            if (wp_verify_nonce($_POST['_wpnonce'], $this->group.'-options'))
            {
                return true;
            } else {
                return false;
            }
        }
    }
    
	public function sanitize_string($input) 
	{
		return $output = htmlspecialchars($input, ENT_QUOTES | ENT_HTML5 | ENT_DISALLOWED, 'UTF-8');
	}
	
	function value()
	{
        if ( $this->is_child_field === TRUE ) return $this->value; // rely on it being set by parent
        global $user_id;
        if( $this->is_user_option || $this->is_user_admin_option )
        {
            $this->value = get_user_option( $this->name, $user_id );
        } else {
            $this->value = get_option( $this->name );
        }
        $this->value = maybe_unserialize( $this->value );
        return $this->value;
	}
    
    function draw( $parent_key = NULL )
    {
        if ($this->is_child_field !== TRUE ) $this->value();
        parent::draw( $parent_key );
    }
    function process_settings()
    {
        switch ($this->type)
        {
            case 'datepicker':
                add_action('admin_enqueue_scripts', array($this, 'register_datepicker'));
            break;
            case 'slider':
                add_action('admin_enqueue_scripts', array($this, 'register_slider'));
            break;
            case 'colorpicker':
                add_action('admin_enqueue_scripts', array($this, 'register_colorpicker'));
            break;
            case 'less_variables':
                add_action('admin_enqueue_scripts', array($this, 'register_colorpicker'));
            break;
            case 'attachment':
                add_action('admin_enqueue_scripts', array($this, 'register_thickbox'));
                add_action('admin_enqueue_scripts', array($this, 'register_attachment'));
            break;
            case 'image':
                add_action('admin_enqueue_scripts', array($this, 'register_thickbox'));
                add_action('admin_enqueue_scripts', array($this, 'register_image'));
            break;
            case 'switch':
                add_action('admin_enqueue_scripts', array($this, 'register_switch'));
            break;
        }
        
        if(($this->repeatable || $this->compound) && $this->is_correct_page() && isset( $_POST['action'] ) && $_POST['action'] == 'update')
        {
            add_action('admin_init', array($this, 'save'));
        }
        if ( is_array( $this->required_scripts ) )
        {
          foreach( $this->required_scripts as $script )
          {
            switch ($script)
            {
              case 'thickbox':
                add_action('admin_enqueue_scripts', array($this, 'register_thickbox'));
              break;
            }
          }
        }
        
        $this->template = $this->type;
        if ( $this->is_child_field === TRUE )
        {
            $this->draw_label = TRUE;
        }
        if (is_admin() && @$_POST['action']=='update' && $this->group == $_POST['option_page'])
        {
            add_action('admin_init', array($this, 'save'));
            
        }
        parent::process_settings();
    }
}