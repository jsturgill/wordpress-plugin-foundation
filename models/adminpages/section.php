<?php
namespace JDSPF\Core\Models\AdminPages;
use JDSPF\Core\Models\Entity as Base;
Class Section extends Base
{

	public $id = 'section_id'; // for css and identification purposes
	public $title = 'section_title'; // html displayed to left of setting
	public $parent_page_slug = 'unset';
	public $html;
	public $settings = array();
	public $options_group = 'default_options_group'; // set this before creating settings if using "create_setting" factory method
	public $is_admin_option_section;
	private $admin_init_action_flag = FALSE;

	function __construct($id, $title, $parent_page_slug)
	{
		$this->id = sanitize_title($id);
		$this->title = $title;
		$this->parent_page_slug = $parent_page_slug;
		add_action('admin_init', array($this, 'register'));
	}
	
	public function register()
	{
        if(($this->is_admin_option_section && current_user_can('edit_users')) || 
            ! $this->is_admin_option_section)
        {
            
            add_settings_section($this->id, $this->title, array($this, 'draw'), $this->parent_page_slug);
        }
	}
	
	public function draw()
	{
        if ( isset($this->has_list_box ) && $this->has_list_box)
        {
            $this->list_box->draw();
        }
		echo $this->html;
	}
	
	public function add_setting($setting_object) {
		$this->settings[] = $setting_object;
		if ($this->admin_init_action_flag === FALSE) 
		{
            add_action('admin_init', array($this, 'register_fields'));
			$this->admin_init_action_flag === TRUE;
		}
	}
    
    function process_settings()
    {
        // nothing for now
    }
	public function register_fields()
	{
		foreach($this->settings as $setting)
		{
            if( is_object( $setting ) && ( !isset( $setting->is_child_field ) || $setting->is_child_field !== TRUE ) )
            {
                $callback = '\JDSPF\Core\Functions\draw_setting_callback';
                $array = array( 'object' => $setting );
                add_settings_field($setting->id, $setting->title, $callback, $this->parent_page_slug, $this->id, $array);
            }
		}
	}
}