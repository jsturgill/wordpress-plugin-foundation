<?php
namespace JDSPF\Core\Models\AdminPages;
use JDSPF\Core\Functions;
use JDSPF\Core\Models\Entity as Base;
class Page extends Base
{
        // where and how to display
        public $count = 0;
        public $menu_type = "main"; // valid options: main, sub, options, and profile
        public $parent_menu; // set only if menu_type = sub
        public $icon_url; // use only if menu_type = main
        public $menu_priority; // used only if menu_type = main;
        public $title = "page_title";
        public $label = "page_label";
        // primary attributes
        public $capability; 
        public $slug = "page_slug";
        public $sections = array();
        public $options_group;
        public $remove_color_scheme_option;
        public $filters;
        public $is_child_field;
        public $name;
        //public $repeatable;
        const DEFAULT_CAPABILITY = 'edit_others_posts';
        const DEFAULT_MENU_TYPE = 'main';
        
        // primitive templating system
        //public $relative_path_to_templates = '../../../views/';// trailing slash only; relative to root plugin directory
        //public $absolute_path_to_self;
        
        // template information
        public $template = null;
        const OBJECT_VARIABLE_NAME = 'page';
        const TEMPLATE_PATH = 'pages/';

        function __construct($slug, $options_group) {
            //$this->absolute_path_to_self = plugin_dir_path(__FILE__);
            $this->slug = $slug;
            $this->page_slug = $slug;
            $this->capability = $this::DEFAULT_CAPABILITY;
            $this->options_group = $options_group;
            $this->menu_type = $this::DEFAULT_MENU_TYPE;
            add_action('admin_menu', array($this, 'register'));
            add_action('admin_enqueue_scripts', array($this, 'register_css'));
        }
                
        function create_section($id, $title) { // an internal factory to create section objects
            $new_section = new OptionsPageSection($id, $title, $this->slug);
            $new_section->options_group = $this->options_group;
            $this->sections[] = $new_section;
            return $new_section;
        }
        
        function set_menu_type($type){
            $this->menu_type = $type;
            $this->template = $type;
        }
        
        function process_settings()
        {
            
            if($this->menu_type && !$this->template) $this->template = $this->menu_type;
            if($this->remove_color_scheme_option && $this->is_correct_page())
            {
                global $_wp_admin_css_colors;
                $object = $this;
                $callback = function() use(&$_wp_admin_css_colors, &$object)
                    {
                        global $user_id;
                        if($object->is_correct_page() && Functions\user_has_role($object->slug, $user_id))
                        {
                            $_wp_admin_css_colors = 0;
                        }
                    };
                add_action('admin_head', $callback);
            }
            parent::process_settings();
        }
        
        function register() {
            if ($this->menu_type == 'sub' && $this->parent_menu !== NULL ) {
                add_submenu_page($this->parent_menu, $this->title, $this->label, $this->capability, $this->slug, array($this, 'draw'));
            } elseif ($this->menu_type == 'option') {
                add_options_page($this->title, $this->label, $this->capability, $this->slug, array($this, 'draw'));
            } elseif ($this->menu_type == 'profile') {
                add_action('profile_personal_options', array($this, 'draw_personal_options')); // top
                add_action('profile_update', array($this, 'save_personal_options'));
                add_action('edit_user_profile', array($this, 'draw_personal_options')); //bottom :(
            } else {
                add_menu_page($this->title, $this->label, $this->capability, $this->slug, array($this, 'draw'), $this->icon_url, $this->menu_priority);
            }
        }
        
        // should eventually specify permission per section to update and display
        // both personal options and regular sections of the page.
        function save_personal_options()
        {
            $current_user = wp_get_current_user();
            if (wp_verify_nonce($_POST['_wpnonce'], 'update-user_' . $_POST['user_id']) &&
                    Functions\user_has_role($this->slug, $_POST['user_id']) && // checks submitted user, not current user
                    (current_user_can('edit_users') || $current_user->id == $_POST['user_id']))
            {
                foreach($this->sections as $section)
                {
                    $test_string = "{$this->slug}_personal_options";
                    if(substr($section->id, 0, strlen($test_string)) == $test_string)
                    {
                        foreach($section->settings as $setting)
                        {
                            $value = call_user_func($setting->validator, $_POST[$setting->name]);
                            update_usermeta($_POST['user_id'], $setting->name, $value);
                        }
                    }
                    $test_string = "{$this->slug}_admin_options";
                    if(substr($section->id, 0, strlen($test_string)) == $test_string &&
                        current_user_can('edit_users'))
                    {
                        foreach($section->settings as $setting)
                        {
                            $value = call_user_func($setting->validator, $_POST[$setting->name]);
                            update_usermeta($_POST['user_id'], $setting->name, $value);
                        }
                    }
                }
            }
        }
        
        function draw_personal_options()
        {
            $this->template = 'personal_options';
            echo $this->draw();
        }
        
        function draw() {
            echo $result = $this->load_template();
        }
}