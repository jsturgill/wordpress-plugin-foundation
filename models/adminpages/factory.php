<?php
namespace JDSPF\Core\Models\AdminPages;
class Factory
{
	public $options_group;
	
	function __construct($options_group, $core_folder, $plugin_folder, $root_path)
	{
        $core_path = $root_path.$core_folder;
        $plugin_path = $root_path.$plugin_folder;
		$this->options_group = $options_group;
        $this->core_path = $core_path;
        $this->plugin_path = $plugin_path;
        $this->root_path = $root_path;
        $this->core_folder = $core_folder;
        $this->plugin_folder = $plugin_folder;
	}

    public function create_list($section_object, $name)
    {
        $new_list_box = new ListBox();
        $section_object->list_box = $new_list_box;
        $section_object->has_list_box = true;
        return $new_list;
    }
    
	public function create_setting($section_object, $name) 
	{
		$new_setting = new Field($this->options_group, $name);
        $section_object->add_setting($new_setting);
        $new_setting->core_path = $this->core_path;
        $new_setting->page_slug = $this->page_slug;
        $new_setting->plugin_path = $this->plugin_path;
        $new_setting->root_path = $this->root_path;
        $new_setting->core_folder = $this->core_folder;
        $new_setting->plugin_folder = $this->plugin_folder;
        $new_setting->page_menu_type = $section_object->page_menu_type;
		return $new_setting;
	}
    
    public function create_child_setting($section_object, $name, $parent_setting) 
	{
		$new_setting = $this->create_setting($section_object, $name);
        $parent_setting->children[] = $new_setting;
        if ( is_array( $parent_setting->parents ) )
        {
        	$new_setting->parents = $parent_setting->parents;
        }
        $new_setting->parents[] =& $parent_setting;
        if ( $parent_setting->name_prefix )
        {
            $new_setting->name_prefix = $parent_setting->name_prefix . '_' . $parent_setting->name;
        } else {
            $new_setting->name_prefix = $parent_setting->name;
        }
        $new_setting->is_child_field = TRUE;
        $parent_setting->compound = true;
		return $new_setting;
	}
	
	function create_section($page_object, $id, $title) 
	{
		$new_section = new Section($id, $title, $page_object->slug);
		$new_section->options_group = $this->options_group;
        $new_section->page_slug = $this->page_slug;
		$page_object->sections[] = $new_section;
        $new_section->core_path = $this->core_path;
        $new_section->plugin_path = $this->plugin_path;
        $new_section->root_path = $this->root_path;
        $new_section->core_folder = $this->core_folder;
        $new_section->plugin_folder = $this->plugin_folder;
        $new_section->page_menu_type = $page_object->menu_type;
		return $new_section;
	}
	
	function create_options_page($slug)
	{
		$new_page = new Page($slug, $this->options_group);
        $this->page_slug = $new_page->slug;
        $new_page->core_path = $this->core_path;
        $new_page->plugin_path = $this->plugin_path;
        $new_page->root_path = $this->root_path;
        $new_page->core_folder = $this->core_folder;
        $new_page->plugin_folder = $this->plugin_folder;
        return $new_page;
    }
}