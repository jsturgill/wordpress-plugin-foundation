<?php
namespace JDSPF\Core\Models\Data;
class DataObject
{
    public $chain = array();
    public $data = array();
    public $trim;

    function __construct( $data = '', $trim = true )
    {
        $this->trim = (bool)$trim;
        $this->data = ( is_array( $data ) ) ? $data : array( $data );
        $callback = function( &$value, $key )
        {
            $value = maybe_unserialize( $value );
        };
        array_walk_recursive( $this->data, $callback );
    }
    
    function __call( $name, $arguments )
    {
        if ( empty( $this->chain ) && method_exists( $this, $name ) )
        {
            call_user_func_array( $this->$name, $arguments );
        } else {
            $this->chain[] = $name;
            $default = ( isset( $arguments[0] ) ) ? $arguments[0] : '';
            $link = &$this->data;
            foreach ( $this->chain as $index=>$key )
            {
                // success route
                if ( is_object( $link ) && isset( $link->$key ) ) {
                    $link = $link->$key;
                } elseif ( is_array( $link ) && isset( $link[$key] ) )
                {
                    $link = &$link[$key];
                } elseif( is_array( $link ) && isset( $link[0] ) && isset( $link[0][$key] ) ) {
                    $link = &$link[0][$key];
                } else {
                    $this->reset();
                    return $default;
                }
            }
            // after following the chain...
            $this->reset();
            if ( is_array($link) && isset( $link[0] ) )
            {
                $copy = ( $this->trim ) ? array_shift( $link ) : $link[0] ;
                $child = ( is_array( $copy ) ) ? $this->spawn( $copy ) : $copy;
                return $child;
            } elseif ( is_array( $link ) && !empty( $link ) ) {
                $child = ( is_array( $link ) ) ? $this->spawn( $link ) : $link;
                return $child;
            } elseif ( is_object( $link ) ) {
                return $link;
            } elseif( isset( $link ) && $link != null ) {
                return $link;
            } else {
                return $default;
            }
        }
    }
    
    function reset()
    {
        $this->chain = array();
    }
    
    function spawn( $new_data )
    {
        $child = clone $this;
        $child->data = ( is_array( $new_data ) ) ? $new_data : array( $new_data );
        $child->chain = array();
        return $child;
    }
    
    function __get( $name )
    {
        $this->chain[] = $name;
        return $this;
    }
}