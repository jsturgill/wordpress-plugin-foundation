<?php
namespace JDSPF\Core\Functions;
use JDSPF\Core\Models\AdminPages;
use JDSPF\Core\Models\PostTypes;
use JDSPF\Core\Models\Taxonomies;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;

// start sessions!
function init_sessions() {
    if (!session_id()) {
        session_start();
    }
}

if ( function_exists( 'add_action' ) )
{
    add_action('init', 'JDSPF\Core\Functions\init_sessions');
    // WordPress does not use sessions in any way.
}

function load_yaml_parser($core_path)
{
    $obj_path = 'lib/symfony_yaml/';
    require_once($core_path.$obj_path.'Yaml.php');
    require_once($core_path.$obj_path.'Parser.php');
    require_once($core_path.$obj_path.'Dumper.php');
    require_once($core_path.$obj_path.'Escaper.php');
    require_once($core_path.$obj_path.'Inline.php');
    require_once($core_path.$obj_path.'Unescaper.php');
    require_once($core_path.$obj_path.'Exception/ExceptionInterface.php');
    require_once($core_path.$obj_path.'Exception/ParseException.php');
    require_once($core_path.$obj_path.'Exception/DumpException.php');
}

function load_admin_models($core_path)
{
    
    // load base objects
    $obj_path = 'models/';
    require_once($core_path.$obj_path.'entity_base.php');
    require_once($core_path.$obj_path.'plugin.php');
    
    // pages
    $obj_path = 'models/adminpages/';
    require_once($core_path.$obj_path.'factory.php');
    require_once($core_path.$obj_path.'page.php');
    require_once($core_path.$obj_path.'section.php');
    require_once($core_path.$obj_path.'field.php');
    //require_once($core_path.$obj_path.'listbox.php');
	
    // post types
	$obj_path = 'models/posttypes/';
	require_once($core_path.$obj_path.'factory.php');
	require_once($core_path.$obj_path.'type.php');
    require_once($core_path.$obj_path.'metabox.php');
    require_once($core_path.$obj_path.'field.php');
    
    // taxonomies
	$obj_path = 'models/taxonomies/';
	require_once($core_path.$obj_path.'factory.php');
	require_once($core_path.$obj_path.'taxonomy.php');

    // data helpers
    $obj_path = 'models/data/';
    require_once($core_path.$obj_path.'dataobject.php');
}

function process_roles_manifest($config_array, $core_folder, $plugin_folder, $root_path)
{
    foreach($config_array as $roles_entry)
    {
        // remove and then add role (clean slate every time)
        $role = $roles_entry['Role'];
        $display_name = ($roles_entry['Display Name']) ? $roles_entry['Display Name'] : $role;
        $capabilities = ( @is_array( $roles_entry['Capabilities'] ) ) ? $roles_entry['Capabilities'] : array();
        //remove_role($role);
        //$result = add_role($role, $display_name, $capabilities);
        
        // create the factory
        $factory = new AdminPages\Factory("{$role}_options", $core_folder, $plugin_folder, $root_path);
        
        // create the page
        $page = $factory->create_options_page("$role");
        $roles_entry['Flags']['menu_type']= 'profile';
        assign_model_settings($page, $roles_entry['Flags']);
        if(array_key_exists('Personal Options', $roles_entry) && is_array($roles_entry['Personal Options']))
        {
            foreach($roles_entry['Personal Options'] as $section_entry)
            {
                $section = $factory->create_section($page, "{$role}_personal_options_{$section_entry['Section']}", $section_entry['Section']);
                foreach ($section_entry['Settings'] as $setting_entry)
                {
                    $setting_entry['is_user_option'] = true;
                    process_setting_entry( $setting_entry, $factory, $section );
                }
            }
        }
        if(array_key_exists('Admin Options', $roles_entry) && is_array($roles_entry['Admin Options']))
        {
            foreach($roles_entry['Admin Options'] as $section_entry)
            {
                $section = $factory->create_section($page, "{$role}_admin_options_{$section_entry['Section']}", $section_entry['Section']);
                $section->is_admin_option_section = true;
                foreach ($section_entry['Settings'] as $setting_entry)
                {
                    $setting_entry['is_user_admin_option'] = true;
                    process_setting_entry( $setting_entry, $factory, $section );
                }
            }
        }
        //pre_print_r($page);
    }
}

function process_shortcodes_manifest($config_array, $core_folder, $plugin_folder, $root_path)
{
    foreach($config_array as $shortcode_entry)
    {
        $path = $root_path.$plugin_folder.'views/shortcodes/'.$shortcode_entry['Name'].'.php';
        if(file_exists($path)){
            $callback = function($args, $content = null) use ($path, $shortcode_entry)
            {
                if ( array_key_exists('Args', $shortcode_entry) 
                     && is_array( $shortcode_entry['Args'] ) ) 
                {
                  extract(shortcode_atts($shortcode_entry['Args'], $args));
                }
                ob_start();
                include($path);
                $result = ob_get_clean();
                return $result;
            };
            add_shortcode($shortcode_entry['Name'], $callback);
        }
    }
}

function process_cron_intervals_manifest( $config_array, $core_folder, $plugin_folder, $root_path )
{
    $callback = function( $schedule = array() ) use ( $config_array ) {
        foreach ( $config_array as $interval )
        {
            $schedule[$interval['Slug']] = array(
                'interval' => (int)$interval['Duration'],
                'display' => $interval['Display'],
             );
        }
        return $schedule;
    };
        add_filter( 'cron_schedules', $callback );
}

function process_cron_jobs_manifest( $config_array, $core_folder, $plugin_folder, $root_path )
{
    foreach( $config_array as $cron_job_entry )
    {
        if ( !wp_next_scheduled( $cron_job_entry['Slug'] ) )
        {
            wp_schedule_event( time(), $cron_job_entry['Interval'], $cron_job_entry['Slug'] );
        }
        add_action( $cron_job_entry['Slug'], $cron_job_entry['Function'] );
    }
}

function process_image_sizes_manifest($config_array, $core_folder, $plugin_folder, $root_path)
{
    foreach ($config_array as $size_entry)
    {
        add_image_size($size_entry['Name'], $size_entry['Width'], 
            $size_entry['Height'], (bool)$size_entry['Crop']);
        if ( isset( $size_entry['List Size'] ) )
        {
            //$values = $size_entry;
            $callback = function($sizes) use ($size_entry)
            {
                $size_entry['Title'] = ($size_entry['Title']) ? $size_entry['Title'] : $size_entry['Name'];
                $to_add = array($size_entry['Name']=>$size_entry['Title']); 
                $new_sizes = array_merge($sizes, $to_add); 
                return $new_sizes;
            };
            add_filter('image_size_names_choose', $callback);
        }
    }
}

function assign_model_settings(&$model, &$settings_array)
{
    foreach ($settings_array as $setting=>$value)
    {
        $setting = str_replace(' ', '_', strtolower(trim($setting)));
        if($setting == 'source')
        {
            $value = str_replace($needle='[core_path]', $replace=\JDSPF\Core\Models\Core::$foundation_path, $haystack=$value);
        }
        if(is_array($model->setting_blacklist) && !array_key_exists($setting, $model->setting_blacklist) && $setting != 'setting_blacklist')
        {
            $model->$setting = $value;
        }
    }
    $model->process_settings();
}

function process_taxonomies_manifest($config_array, $core_folder, $plugin_folder, $root_path)
{
    foreach ($config_array as $taxonomy_entry)
    {
        $factory = new Taxonomies\Factory($core_folder, $plugin_folder, $root_path);
        $taxonomy = $factory->create_taxonomy($taxonomy_entry['Name']);
        assign_model_settings($taxonomy, $taxonomy_entry);
    }
}

function process_meta_boxes_manifest($config_array, $core_folder, $plugin_folder, $root_path)
{
    foreach($config_array as $meta_box_entry)
    {
        $factory = new PostTypes\Factory($core_folder, $plugin_folder, $root_path);
        $meta_box = $factory->create_meta_box($meta_box_entry['Post Type'], $meta_box_entry['ID']);
        assign_model_settings($meta_box, $meta_box_entry);
        
        if (array_key_exists('Fields', $meta_box_entry))
        {
            foreach($meta_box_entry['Fields'] as $field_entry)
            {
                process_field_entry($factory, $meta_box, $field_entry);
            }
        }
    }
}

function process_post_types_manifest($config_array, $core_folder, $plugin_folder, $root_path)
{
	foreach($config_array as $type_entry)
	{
		$factory = new PostTypes\Factory($core_folder, $plugin_folder, $root_path);
		$type = $factory->create_type($type_entry['Slug']);
		//supports, crud roles, and editor roles must all be arrays!
        assign_model_settings($type, $type_entry);
        
        foreach ($type_entry['Meta Boxes'] as $meta_box_entry)
        {
            $meta_box = $factory->create_meta_box($type, $meta_box_entry['ID']);
            assign_model_settings($meta_box, $meta_box_entry);
            
            foreach($meta_box_entry['Fields'] as $field_entry)
            {
                process_field_entry($factory, $meta_box, $field_entry);
            }
        }
	}
}

function process_field_entry( $factory, $meta_box, $field_entry )
{
    $type = (array_key_exists('Type', $field_entry)) ? $field_entry['Type'] : null;
    $field = $factory->create_field($meta_box, $field_entry['Name'], $type);
    assign_model_settings($field, $field_entry);
    if ( is_array( $field->child_fields ) )
    {
        foreach( $field->child_fields as $child_field_entry )
        {
            process_child_field_entry( $factory, $meta_box, $child_field_entry, $parent = $field );
        }
    }
}

function process_child_field_entry( $factory, $meta_box, $field_entry, $parent_field )
{
    $type = (array_key_exists('Type', $field_entry)) ? $field_entry['Type'] : null;
    $field = $factory->create_child_field($meta_box, $field_entry['Name'], $type, $parent_field);
    assign_model_settings($field, $field_entry);
    if ( is_array( $field->child_fields ) )
    {
        foreach( $field->child_fields as $child_field_entry )
        {
            process_child_field_entry( $factory, $meta_box, $child_field_entry, $parent = $field );
        }
    }
}

function process_setting_entry( $setting_entry, $factory, $section )
{
    $type = ( @$setting_entry['Type'] ) ? $setting_entry['Type'] : 'textbox';
    $validator = ( isset( $setting_entry['Validator'] ) && $setting_entry['Validator'] ) ? $setting_entry['Validator'] : null;
    $setting = $factory->create_setting($section, $setting_entry['Name']);
    assign_model_settings($setting, $setting_entry);
    if ( is_array( $setting->child_fields ) )
    {
        foreach( $setting->child_fields as $child_field_entry )
        {
            process_child_setting_entry( $child_field_entry, $factory, $section, $parent = $setting );
        }
    }
}

function process_child_setting_entry( $setting_entry, $factory, $section, $parent_setting )
{
    $type = ($setting_entry['Type']) ? $setting_entry['Type'] : 'textbox';
    $validator = ( isset( $setting_entry['Validator'] ) && $setting_entry['Validator'] ) ? $validator : null;
    $setting = $factory->create_child_setting($section, $setting_entry['Name'], $parent_setting);
    assign_model_settings($setting, $setting_entry);
    if ( is_array( $setting->child_fields ) )
    {
        foreach( $setting->child_fields as $child_field_entry )
        {
            process_child_setting_entry( $child_field_entry, $factory, $section, $parent = $setting );
        }
    }
}

function process_admin_menu_pages_manifest($config_array, $core_folder, $plugin_folder, $root_path)
{
    foreach($config_array as $page_entry)
    {
        $factory = new AdminPages\Factory($page_entry['Options Group'], $core_folder, $plugin_folder, $root_path);
        $page = $factory->create_options_page($page_entry['Slug']);
        assign_model_settings($page, $page_entry);
        foreach($page_entry['Sections'] as $section_entry)
        {
            $section = $factory->create_section($page, $section_entry['ID'], $section_entry['Title']);
            assign_model_settings($section, $section_entry);
            if(array_key_exists('Settings', $section_entry)) {
                foreach($section_entry['Settings'] as $setting_entry)
                {
                    process_setting_entry( $setting_entry, $factory, $section );
                }
            }
            if(array_key_exists('List', $section_entry))
            {
                $callback = function() use ($section_entry, $section, $factory)
                {
                    $factory->create_list($section, $section_entry['List']);
                };
                add_action('admin_init', $callback);
            }
        }
    }
}

function bootstrap($plugin_folder, $core_folder, $root_path)
{
    $core_path = $root_path.$core_folder;
    $plugin_path = $root_path.$plugin_folder;
    
    if ( file_exists($plugin_path.'functions.php') )
    {
        require_once($plugin_path.'functions.php');
    }
    // Load Symfony YAML component (dependency).
    load_yaml_parser($core_path);

    // Load internal plugin Models
    load_admin_models($core_path);
    
    $manifest_of_manifests = array(
        'admin_menu_pages',
        'post_types',
        'taxonomies',
        'image_sizes',
        'meta_boxes',
        'shortcodes',
        'roles',
        'cron_intervals',
        'cron_jobs',
    );
    
    // Load manifests
    foreach($manifest_of_manifests as $manifest)
    {
        if(file_exists("{$plugin_path}manifests/$manifest.yaml"))
        {
            $manifests[$manifest] = file_get_contents("{$plugin_path}manifests/$manifest.yaml");
        }
    }
    
    if ( !@is_array( $manifests ) )
    {
        return;
    }

    // Uncomment to view error message example
    //$manifests['admin_pages'] = "[-sadf{{{dsafdfdsa{[";

    // Convert from YAML to arrays
    foreach ( $manifests as $name=>$contents )
    {
        try {
            $manifests[$name] = Yaml::parse($contents);
        } catch (ParseException $e) {
            //printf("Malformed config for Events Plugin $name: %s", $e->getMessage());
            if (is_admin())
            { $style = "padding:.5em; margin:0; background:salmon; border:red; font-size:110%;";
                    echo "<p style='$style'>Malformed manifest detected! <br />Review the <strong>$name file</strong> at <strong>{$plugin_path}manifests/$name.yaml</strong></p>";
            } else {
                error_log("Malformed manifest detected! Review the $name file at {$plugin_path}manifests/$name.yaml");
            }
        }
    }
    
    
    // Process manifests
    foreach ($manifests as $manifest=>$config_array)
    {
        if(is_array($config_array))
        {
            $function = "JDSPF\Core\Functions\process_{$manifest}_manifest";
            $function($config_array, $core_folder, $plugin_folder, $root_path);
        }
    }
    
}

function pass_input($input)
{
    return $input;
}

function user_has_role($role_name, $id = null)
{
    if($id)
    {
        $user = get_user_by('id', (int)$id);
    }else{
        $user = wp_get_current_user();
    }
    $result = false;
    foreach ($user->roles as $key=>$role)
    {
        if ($role_name == $role)
        {
            $result = true;
        }
    }
    return $result;
}

function pre_print_r($thing)
{
    echo '<pre>';
    print_r($thing);
    echo '</pre>';
}

function call_view_helper(&$helper_function, &$helper_variable)
{
    $result = call_user_func($helper_function, $helper_variable);
    return $result;
}

function trim_array($string_or_array)
{
    if(is_array($string_or_array))
    {
        foreach($string_or_array as $key=>$val)
        {
            $string_or_array[$key] = trim_array($val);
        }
        return $string_or_array;
    } else {
        return trim($string_or_array);
    }
}

function draw_setting_callback( $array )
{
    $temp = $array['object'];
    if ( is_object( $temp ) ) 
    {
        $temp->draw();
    }
}

function sort_post_custom( $custom_array )
{
	
	foreach( $custom_array as $key=>$value )
	{
		$value = maybe_unserialize( $value );
		if ( is_array( $value ) )
		{
			foreach ( $value as $child_key=>$child_value )
			{
				$value[$child_key] = maybe_unserialize( $child_value );
			}
		}
		if( is_array( $value ) && isset( $value[0] ) && is_array( $value[0] ) 
            && isset( $value[0]['_sort_order'] ) )
        {
		  $custom_array[$key] = sort_field_values( $value );
        }
	}
	return $custom_array;
}

function sort_field_values( $values_array )
{
	if ( is_array( $values_array ) && isset( $values_array[0] ) && is_array( $values_array[0] ) && array_key_exists( '_sort_order', $values_array[0] ) )
	{
		$sort = function( $a, $b )
		{
			$a_order = $a['_sort_order'];
			$b_order = $b['_sort_order'];
			if ( $a_order == $b_order ) 
			{
				return 0;
			}
	   		return ( $a_order < $b_order ) ? -1 : 1;
		};
		uasort( $values_array, $sort );
	}
	return $values_array;
}