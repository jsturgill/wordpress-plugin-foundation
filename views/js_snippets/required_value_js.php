<script type="text/javascript">
    jQuery(document).ready( function($) {
    if (jQuery('#<?=esc_attr( $check_target );?>').val() == '<?=esc_attr($value);?>') {
        jQuery('#<?=esc_attr( $metabox );?>').show();
    } else {
        jQuery('#<?=esc_attr( $metabox );?>').hide();
    }
    // Debug only
    // - outputs the template filename
    // - checking for console existance to avoid js errors in non-compliant browsers
    if (typeof console == "object") 
        console.log ('default value = ' + jQuery('#<?=esc_attr( $check_target );?>').val());
        
    jQuery('#<?=esc_attr( $check_target );?>').live('change', function(){
            if(jQuery(this).val() == '<?=esc_attr($value);?>') {
            // show the meta box
            jQuery('#<?=esc_attr($metabox);?>').show();
        } else {
            // hide your meta box
            jQuery('#<?=esc_attr($metabox);?>').hide();
        }

        // Debug only
        if (typeof console == "object") 
            console.log ('live change value = ' + jQuery(this).val());
    });    
});    
</script>