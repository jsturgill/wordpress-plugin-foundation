<script type="text/javascript">
    jQuery(document).ready(function($) {
        function set_slider(target)
        {
            $(target).parent().find('.<?=$field_name;?>_slider').each(function()
            {
                $(this).slider(
                {
                    value: $(target).val(),  
                    min: <?=$slider_min;?>,  
                    max: <?=$slider_max;?>,  
                    step: <?=$slider_step;?>,
                    slide: function( event, ui ) 
                    {  
                        $(target).val( ui.value );
                    }
                });
            });
        }
    
        jQuery(document).ready(function() 
        {
            jQuery(document).on('focus blur', ".jds_core_slider", function()
            {
                set_slider(this);
            });
            jQuery(".jds_core_slider").each(function()
            {
                set_slider(this);
            });
        });
    });
</script>