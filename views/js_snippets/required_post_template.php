<script type="text/javascript">
    jQuery(document).ready( function($) {
    if (jQuery('#page_template').val() == '<?=esc_attr($filename);?>') {
        jQuery('#<?=esc_attr($target);?>').show();
    } else {
        jQuery('#<?=esc_attr($target);?>').hide();
    }
    // Debug only
    // - outputs the template filename
    // - checking for console existance to avoid js errors in non-compliant browsers
    if (typeof console == "object") 
        console.log ('default value = ' + jQuery('#page_template').val());
        
    jQuery('#page_template').live('change', function(){
            if(jQuery(this).val() == '<?=esc_attr($filename);?>') {
            // show the meta box
            jQuery('#<?=esc_attr($target);?>').show();
        } else {
            // hide your meta box
            jQuery('#<?=esc_attr($target);?>').hide();
        }

        // Debug only
        if (typeof console == "object") 
            console.log ('live change value = ' + jQuery(this).val());
    });    
});    
</script>