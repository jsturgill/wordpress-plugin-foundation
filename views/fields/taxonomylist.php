<?php
$taxonomylist = get_terms( $field->source, array ( 'offset' => 0,));

if (empty($field->value) || ! is_array($field->value)) {
    $field->value = array(0=>$field->value); // prime the pump...
}
foreach($field->value as $key=>$value) : ?>
<div class="<?=$field->draw_field_container_classes( $key );?>" id="<?=$field->draw_field_container_id($key);?>">
    <select class="<?=$field->draw_input_classes();?>" id="<?=$field->draw_input_id($key);?>"
    name="<?=$field->draw_field_name($key);?>">
        <option value=""><?=($field->select_label) ? $field->select_label : 'Select an entry:';?></option>
        <?php
            foreach($taxonomylist as $option) 
            {   
                echo '<option value="' . esc_attr( $option->slug ) . '"' . (($value == esc_attr( $option->slug ) ) ? ' selected="selected"' : '') . '>'.esc_html( $option->name ).'</option>';
            }
        ?>
    </select>
    <?php $field->draw_remove_button( $key ); ?>
</div>
<?php endforeach;