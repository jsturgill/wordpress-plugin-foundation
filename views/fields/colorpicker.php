<?php
    if (empty($field->value) || ! is_array($field->value)){
        $field->value = array(0=>$field->value); // prime the pump...
    }
    foreach($field->value as $key=>$value) : ?>
    <div class="<?=$field->draw_field_container_classes( $key );?>" id="<?=$field->draw_field_container_id( $key );?>">
        <input class="<?=$field->draw_input_classes('color');?>" id="<?=$field->draw_input_id( $key );?>"
            name="<?=$field->draw_field_name($key);?>" type="text" maxlength="6" 
            value="<?=esc_attr($value);?>" />
        <?php $field->draw_remove_button( $key ); ?>
        <script type="text/javascript">
            // re-initialize jscolor so that it binds to the new
            // element. This javascript will be carried over
            // when the container is copied, and it should fire
            // every time.
            jscolor.init();
        </script>
    </div>
    <?php endforeach;