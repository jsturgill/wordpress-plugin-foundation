<?php 
if (empty($field->value) || ! is_array($field->value)){
    $field->value = array(0=>$field->value); // prime the pump...
}
foreach($field->value as $key=>$value) : ?>
    <div class="<?=$field->draw_field_container_classes( $key );?>" id="<?=$field->draw_field_container_id($key);?>">
        <input id="<?=$field->draw_input_id($key);?>" readonly class="<?=$field->draw_input_classes('readonly');?>" type="text" name="<?=$field->draw_field_name($key);?>" value="<?=esc_attr($value);?>" />  
        <input id="<?=$field->draw_input_id($key);?>_upload_button" class="button jds_core_button <?=$field->name;?>_repeat_update attachment_upload_button" type="button" id="<?=$field->draw_input_id($key);?>_upload_button" value="Upload" /> &nbsp; <a href="#" class="attachment_clear_button <?=$field->name;?>_repeat_update small" id="<?=$field->draw_input_id($key);?>_clear_button">clear selection</a>
        <?php $field->draw_remove_button( $key ); ?>
    </div>
    <?php
endforeach;