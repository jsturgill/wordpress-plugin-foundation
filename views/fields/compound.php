<?php 
if (empty($field->value) || ! is_array($field->value)){
    $field->value = array(0=>$field->value); // prime the pump...
}
foreach($field->value as $key=>$value) : ?>
    <div class="<?=$field->draw_field_container_classes( $key );?>" id="<?=$field->draw_field_container_id($key);?>">
        <?php
        foreach( $field->children as $child )
        {
            $child->value = @$value[$child->name];
            $child->draw($key);
        }
        $field->draw_remove_button( $key ); ?>
    </div>
<?php endforeach;
