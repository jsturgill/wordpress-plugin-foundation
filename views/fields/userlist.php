<?php
$userlist = get_users( array (   
    'role' => $field->source
));
//JDSPF\Core\Functions\pre_print_r($userlist);
if (empty($field->value) || ! is_array($field->value)) {
    $field->value = array(0=>$field->value); // prime the pump...
}
foreach($field->value as $key=>$value) : ?>
<div class="<?=$field->draw_field_container_classes( $key );?>" id="<?=$field->draw_field_container_id($key);?>">
    <select class="<?=$field->draw_input_classes();?>" id="<?=$field->draw_input_id($key);?>"
    name="<?=$field->draw_field_name($key);?>">
        <option value=""><?=($field->select_label) ? $field->select_label : 'Select a user:';?></option>
        <?php
            foreach($userlist as $option) 
            {
                echo '<option value="' . esc_attr($option->ID) . '" ' .
                     (($value == $option->ID) ? ' selected="selected"' : '') . 
                     '>' . esc_attr($option->ID) . ' - ' . esc_attr($option->user_login) . ' (' . 
                     esc_attr($option->user_email) . ')</option>';
            }
        ?>
    </select>
    <?php $field->draw_remove_button( $key ); ?>
</div>
<?php endforeach;