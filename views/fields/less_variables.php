<div class="<?=$field->draw_field_container_classes( $key );?>" id="<?=$field->draw_field_container_id($key);?>">
    <?php 
    /*
     * Read variables into an array.
     * No guarantees as to behavior on malformed files...
     * And there are some other restrictions:  the variable must be at the beginning of a line, 
     * or it won't be picked up.
     */
    $file = fopen( $field->source, r );
    $variables = array();
    if ($file)
    {
        while( ( $buffer = fgets( $file ) ) !== FALSE )
        {
            $buffer = trim($buffer);
            if (substr($buffer, 0, 1) === '@')
            {
                $variable = explode(':', $buffer);
                if (count($variable) >= 2) {
                    $end_of_var = strpos($variable[1], ';'); // false if no semicolon
                    if( $end_of_var ) 
                    {
                        $variable[1] = trim(substr($variable[1], 0, $end_of_var ));
                        $variable[0] = trim(substr($variable[0],1));
                        $variables[] = $variable;
                    }
                }
            }
        }
        fclose( $file );
    }
    if (count($variables)>0)
    {
        echo '<table class="less_variables">';
        foreach($variables as $index=>$variable) :
        if (substr($variable[1], 0, 1) == '#')
        {
            $color_class='color';
        } else {
            $color_class = '';
        }?>
            <tr class="less_variable">
            <td class="less_label"><label class="less_variable"><?=esc_html($variable[0]);?></label></td>
            <td class="less_value"><input type="text" class="<?=$field->draw_input_classes($color_class);?>"
name="<?=$field->draw_field_name();?>[<?=esc_attr($variable[0]);?>]" value="<?=esc_attr($variable[1]);?>" />
<?php $field->draw_remove_button( $key );?></td>
            <td class="less_controls">colorpicker to text | colorpicker | delete | add new variable</td>
        </tr>
        <?php endforeach;
        echo '</table>';

    }
    ?>
    <script type="text/javascript">
        // re-initialize jscolor so that it binds to the new
        // element. This javascript will be carried over
        // when the container is copied, and it should fire
        // every time.
        jscolor.init();
    </script>
</div>