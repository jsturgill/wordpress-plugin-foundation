<?php 
if (empty($field->value) || ! is_array($field->value)){
    $field->value = array(0=>$field->value); // prime the pump...
}
foreach($field->value as $key=>$value) : ?>
    <div class="<?=$field->draw_field_container_classes( $key );?>" id="<?=$field->draw_field_container_id($key);?>">
        <select class="<?=$field->draw_input_classes();?>" id="<?=$field->draw_input_id($key);?>"
        name="<?=$field->draw_field_name($key);?>[_selection]">
        <option value=''>Select a template</option>
        <?php
        // populate the dropdown box
        foreach( $field->children as $child )
        {
            $option_value = esc_attr( $child->name );
            $display = esc_html( $child->title );
            $selected = ( @$value['_selection'] === $option_value ) ? ' selected="true" ' : '';
            echo "<option $selected value='$option_value'>$display</option>";
        }
        ?></select>
        <?php $field->draw_remove_button( $key );?>
    </div>
    <div id="<?=$field->draw_input_id($key);?>_children">
        <?php // draw the fields
        foreach( $field->children as $child )
        {
            $class = esc_attr( $child->name ) . '_holder';
            $style = ( $child->name === @$value['_selection'] ) ? '' : 'style="display:none;"';
            echo "<div class='$class' $style>";
            $child->value = @$value[$child->name];
            $child->draw($key);
            echo '</div>';
        }?>
    </div>
<?php endforeach;