<?php
if (empty($field->value) || ! is_array($field->value)){
    $field->value = array(0=>$field->value); // prime the pump...
}
foreach($field->value as $key=>$value) : ?>
<div class="<?=$field->draw_field_container_classes( $key );?>" id="<?=$field->draw_field_container_id($key);?>">
    <select class="<?=$field->draw_input_classes();?>" id="<?=$field->draw_input_id($key);?>"
        name="<?=$field->draw_field_name($key);?>">  
    <?php 
        if(is_callable($field->source))
        {
            $field->source = \JDSPF\Core\Functions\call_view_helper($field->source, $field);

        } 

        if (is_array($field->source)) {
            if (@$this->selection_prompt)
            {
                echo '<option value="">' . $this->selection_prompt . '</option>';
            } else {
				$selected = ( (string)$value === '' ) ? 'selected="true"': '';
                echo '<option ' . $selected . ' value="">Select an option:</option>';
            }
            foreach ($field->source as $entry)
            {
                $selected = ((string)$value === (string)$entry['value']) ? 'selected="true"': '';
                echo "<option {$selected} value=" . esc_attr($entry['value']) . ">" . esc_html($entry['text']) ."</option>";
            }
        }
    ?>
    </select>
    <?php $field->draw_remove_button( $key ); ?>
</div>
<?php endforeach;