<?php
if (empty($field->value) || ! is_array($field->value)){
    if ($field->value == '') 
    {
      $field->value = @$field->default_value;
    }
    $field->value = array(0=>$field->value); // prime the pump...
}
foreach($field->value as $key=>$value) : 
?>
<div class="<?=$field->draw_field_container_classes( $key );?>" id="<?=$field->draw_field_container_id($key);?>">
    <textarea class="<?=$field->draw_input_classes();?>" id="<?=$field->draw_input_id($key);?>"
    name="<?=$field->draw_field_name($key);?>"><?=esc_attr($value);?></textarea>
    <?php $field->draw_remove_button( $key ); ?>
</div>
<?php endforeach;