<?php
if (empty($field->value) || ! is_array($field->value)){
    $field->value = array(0=>$field->value); // prime the pump...
}
foreach($field->value as $key=>$value) : 
    $value = ((string)$value == '') ? $field->default_value : $value;
?>
<div class="<?=$field->draw_field_container_classes( $key );?>" id="<?=$field->draw_field_container_id($key);?>">
    <input type="text" class="<?=$field->draw_input_classes();?>" id="<?=$field->draw_input_id($key);?>"
    name="<?=$field->draw_field_name($key);?>" value="<?=esc_attr($value);?>" />
    <?php $field->draw_remove_button( $key ); ?>
</div>
<?php endforeach;