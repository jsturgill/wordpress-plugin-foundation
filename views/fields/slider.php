<?php
// TODO: move away from snippet to includable, generic script for all sliders

    if (empty($field->value) || ! is_array($field->value)){
        $field->value = array(0=>$field->value); // prime the pump...
    }
    foreach($field->value as $key=>$value) : ?>
    <div class="<?=$field->draw_field_container_classes( $key );?>" id="<?=$field->draw_field_container_id($key);?>">
        <input type="text" class="<?=$field->draw_input_classes();?>" id="<?=$field->draw_input_id($key);?>"
                name="<?=$field->draw_field_name($key);?>" 
                value="<?php echo (is_numeric($value)) ? $value : $field->slider_min;?>" />
        <?php $field->draw_remove_button( $key ); ?>
        <div class="slider_ui_container"><div id="<?=$field->draw_input_id($key);?>_slider" class="<?=$field->name;?>_slider"></div></div>
    </div>
<?php endforeach;?>
<?php
$field->render_js_snippet('slider_js', array('field_name'=>$field->name, 
                                           'slider_min'=>$field->slider_min,
                                           'slider_max'=>$field->slider_max,
                                           'slider_step'=>$field->slider_step,
                                           'field_value'=>$field->value));