<?php
    if (empty($field->value) || ! is_array($field->value)){
        $field->value = array(0=>$field->value); // prime the pump...
    }
    foreach($field->value as $key=>$value) : $image = '';?>
    <div class="<?=$field->draw_field_container_classes( $key );?>" id="<?=$field->draw_field_container_id($key);?>">
        <?php if ($value) 
        {
            $image = wp_get_attachment_image_src($value, 'medium'); 
            $image = $image[0];
        }?>
        <input class="<?=$field->draw_input_classes();?>" id="<?=$field->draw_input_id($key);?>"
                    name="<?=$field->draw_field_name($key);?>" type="hidden" value="<?=esc_attr($value);?>" />  
        <input id="<?=$field->draw_input_id($key);?>_source" class="jds_core_textbox readonly <?=$field->name;?>_repeat_reset_val <?=$field->name;?>_repeat_update" readonly value="<?=esc_attr($image);?>" type="text" /> 
        <div class="jds_core_image_upload_buttons_container">
            <input id="<?=$field->draw_input_id($key);?>_upload_button" class="jds_core_button image_upload_button button <?=$field->name;?>_repeat_update" type="button" value="Choose Image" />
            <a href="#" id="<?=$field->draw_input_id($key);?>_clear_button" class="image_clear_button small <?=$field->name;?>_repeat_update">clear image</a>
            <?php $field->draw_remove_button( $key ); ?>
        </div>
        <div class="jds_core_preview_container <?=$field->name;?>_preview_container <?=$field->name;?>_repeat_update repeat_hide" id="<?=$field->draw_input_id($key);?>_preview_container" style="<?=($image)?'':'display:none;';?>">
                <p><small>preview</small></p>
                <img src="<?=esc_attr($image);?>" class="<?=$field->name;?>_preview repeat_hide <?=$field->name;?>_repeat_update jds_core_preview_image" id="<?=$field->draw_input_id($key);?>_preview_image" alt="" />
        </div>
    </div>

<?php endforeach;