<?php
$args = array (   
    'post_type' => $field->source,   
    'posts_per_page' => -1
);
$postlist = get_posts( $args );
if (empty($field->value) || ! is_array($field->value)) {
    $field->value = array(0=>$field->value); // prime the pump...
}
foreach($field->value as $key=>$value) : ?>
<div class="<?=$field->draw_field_container_classes( $key );?>" id="<?=$field->draw_field_container_id($key);?>">
    <select class="<?=$field->draw_input_classes();?>" id="<?=$field->draw_input_id($key);?>"
    name="<?=$field->draw_field_name($key);?>">
        <option value=""><?=( @$field->select_label ) ? $field->select_label : 'Select an entry:';?></option>
        <?php
            foreach($postlist as $option) 
            {   
                echo '<option value="' . $option->ID . '"' . (($value == $option->ID) ? ' selected="selected"' : '') . '>'.$option->post_title.'</option>';
            }
        ?>
    </select>
<?php $field->draw_remove_button( $key ); ?>
</div>
<?php endforeach;