<?php
use JDSPF\Core\Functions;
global $user_id;
if(Functions\user_has_role($page->slug, $user_id))
{
    do_settings_sections( $page->slug );
}