<?php
/*
 * This is the default main page template.
 * It will be overwritten when the plugin is updated!
 * To customize, create a new template in the views folder
 * and then specify that template in the appropriate YAML
 * config file.
 */
?>
<div class="wrap">
	<form method="post" action="options.php">
		<?php 
			settings_fields( $page->options_group );
			do_settings_sections( $page->slug );
            submit_button();?>
	</form>
</div>