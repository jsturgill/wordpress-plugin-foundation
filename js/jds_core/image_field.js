jQuery(document).ready(function() {
    jQuery(document).on('click', '.image_upload_button', function() {
        var regex = /_upload_button$/;
        var target = jQuery(this).attr('id');
        target = target.replace(regex, '');
        var default_function = window.send_to_editor;
        window.send_to_editor = function(html) {
            imgurl = jQuery('img',html).attr('src');   
            classes = jQuery('img', html).attr('class');   
            var id = classes.replace(/(.*?)wp-image-/, '');   
            jQuery('#' + target).val(id);
            jQuery('#' + target + '_preview_image').attr('src', imgurl).show();
            jQuery('#' + target + '_preview_container').show();   
            jQuery('#' + target + '_source').val(imgurl);
            tb_remove();
            window.send_to_editor = default_function;
        };
        tb_show('', 'media-upload.php?type=image&TB_iframe=true');   
        return false;
    });   
       
    jQuery(document).on('click', '.image_clear_button', function() {
        var regex = /_clear_button$/;
        var id = jQuery(this).attr('id');
        id = id.replace(regex, '');
        jQuery('#' + id + '_source, #'+ id).val('');
        jQuery('#' + id + '_preview_container').hide();
        jQuery('#' + id + '_preview_image').attr('src', '').hide();   
        return false;   
    });   
});