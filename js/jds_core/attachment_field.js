jQuery(document).ready(function($) {  

    jQuery('.attachment_upload_button').live('click', function(e) {
        var id = jQuery(this).attr('id');
        var regex = /_upload_button$/;
        id = id.replace(regex, '');
        var default_function = window.send_to_editor;
        window.send_to_editor = function(html) {
            var url = jQuery(html).attr('href');
            jQuery('#'+id).val(url);
            tb_remove();  
            window.send_to_editor = default_function;
        };
        tb_show('', 'media-upload.php?TB_iframe=true');         
        return false;  
    });
    jQuery('.attachment_clear_button').live('click', function(e) {
            var id = jQuery(this).attr('id');
            var regex = /_clear_button$/;
            id = id.replace(regex, '');
            jQuery('#'+id).val('');
            return false;   
    });
}); 