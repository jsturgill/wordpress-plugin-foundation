(function($){
	jQuery(document).ready( function()
	{

		function hide_fields( id )
		{
			$('#' + id + '_children' ).children().hide();
		}

		function show_field( switch_id, field_id )
		{
			var field_container_class = field_id + '_holder';
			$('#' + switch_id + '_children' ).children( '.' + field_container_class ).show();	
		}

		$('.jds_core_field_holder').on('change', 'select.jds_core_switch', function() {
	    	var $this = $( this );
	    	var switch_id = $this.attr( 'id' );
	    	var current_field = $this.val();
	    	hide_fields( switch_id );
	    	show_field( switch_id, current_field );
		});
	});
})(jQuery);