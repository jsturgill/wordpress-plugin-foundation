$(document).on( 'input propertychange', 'input.integer_only', function(e){
    var $this = $( this );
    var value = $this.val().replace(/[^0-9]/g, '');
    $this.val( value );
} );