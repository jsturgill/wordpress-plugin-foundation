jQuery(document).ready( function()
{
    function update_name( match_string, element, count )
    {
        var el_id = jQuery( element ).attr( 'id' );
        var id_to_name_regex = /_([0-9]{1,})(_|$)/g; // captures _#_ or _# at the end of the line
        var complete_brackets_regex = /\]([a-zA-Z0-9_-]{1,})\[/g; //completes the name by bracketing unbracketed stuff between brackets.... :) 
        var name = jQuery( element ).attr( 'name' );;
        if ( typeof el_id !== 'undefined' && typeof name !== 'undefined' )
        {
        	name = el_id.replace( id_to_name_regex, "[$1]" );
        	name = name.replace( complete_brackets_regex, "][$1][" );
            jQuery( element ).attr( 'name', name );
        } 
    }
    function update_id(match_string, element, count)
    {
        var regex = new RegExp(match_string + '[0-9]{1,}');
        var el_id = jQuery( element ).attr( 'id' );
        if ( typeof el_id !== 'undefined' && el_id.substring( 0, match_string.length ) === match_string )
        {
            el_id = el_id.replace( regex, match_string + count );
            jQuery( element ).attr( 'id', el_id );
            return true;
        } else {
            return false;
        }
    }

    function reset_numbers( container )
    {
        var match_string = get_repeat_block_id( container );
        //alert( match_string);
        var count = 0;
        var stripe_class;
        jQuery( container ).children( '.repeat_block' ).each(function()
        {
            if ( count % 2 == 0 )
            {
                stripe_class = 'even';
            } else {
                stripe_class = 'odd';
            }
            jQuery( this ).removeClass( 'even' ).removeClass( 'odd' ).addClass( stripe_class );
            update_id( match_string, this, count );
            update_name( match_string, this, count );
            jQuery(this).find( '*' ).each( function()
            {
                update_id( match_string, this, count );
                update_name( match_string, this, count );
            });
            count++;
        });
        reset_first_blocks( container );
    }
    
    function reset_first_blocks( container )
    {
        jQuery( container ).find( '.repeat_block_holder' ).each(function(){
        	jQuery( this ).children('.repeat_block').first().removeClass( 'repeated_block' ).addClass( 'first_block' );
        });
    }
    
    jQuery( document ).on( 'click', '.repeatable_remove_button', function(e) {
        e.preventDefault();
        var repeat_block_holder = jQuery( this ).closest( '.repeat_block_holder' );
        //jQuery( repeat_block_holder ).css( 'border', '1px solid green' );
        var repeat_block_id = jQuery( repeat_block_holder ).children( '.repeat_button_container' ).children('.repeat_button').attr( 'id' );
        if ( jQuery( repeat_block_holder ).children( '.jds_core_field_container' ).length > 1 ) 
        {
            jQuery( this ).closest('.repeat_block').remove();
            reset_numbers( repeat_block_holder );
        }
    });
    
    // pass a jQuery object
    function clear_repeated_el( $repeat_block ) {
        $repeat_block.find( '*' ).not( '.repeat_button, .button' ).not( '.skip_on_repeat' ).each( function()
        {
            if ( jQuery( this ).hasClass( 'first_block' ) )
            {
                jQuery( this ).removeClass( 'first_block' );
            }
            
            if ( jQuery( this ).hasClass( 'repeated_block' ) )
            {
                jQuery( this ).remove();
            }
            
            if (jQuery(this).is(':checkbox'))
            {
                jQuery(this).attr('checked', false);
            } else if ( ! jQuery(this).is('option') ) {
                jQuery(this).val('');
            }
            
            if (jQuery(this).hasClass('repeat_hide'))
            {
                jQuery(this).hide();
            }
            
            if (jQuery(this).hasClass('gets_repeat_focus'))
            {
                jQuery(this).focus();
            }
            
            if ( jQuery( this ).hasClass( 'hasDatepicker' )) 
        	{
            	jQuery( this ).removeClass( 'hasDatepicker' );
    		}
        });
        // were some problems in the past if datepicker class removed before appending
        // keep an eye out for them!
        jQuery( $repeat_block ).find( '.hasDatepicker' ).each( function()
        {
            jQuery( this ).removeClass( 'hasDatepicker' );
        });
        jQuery( $repeat_block ).addClass( 'repeated_block' ).removeClass( 'first_block' );
    }
    
    // pass a jQuery object
    function repeat_a_block( $repeat_block_holder ) {
        var $repeat_block = $repeat_block_holder.find( '.repeat_block' ).first( '.repeat_block' ).clone();
        var last_block = $repeat_block_holder.children( '.repeat_block' ).last( '.repeat_block' );
        clear_repeated_el( $repeat_block );
        jQuery( last_block ).after( $repeat_block );
        reset_numbers( $repeat_block_holder );
    }
    
    // pass a jQuery object
    function get_repeat_block_id( $repeat_block_holder ) {
        var repeat_block_id = $repeat_block_holder.children( '.repeat_button_container' ).children( '.repeat_button' ).attr( 'id' );
        return repeat_block_id;
    }
    
    jQuery( document ).on( 'click', '.repeat_button', function(e) {
        e.preventDefault();
        var repeat_block_holder = jQuery( this ).closest( '.repeat_block_holder' );
        repeat_a_block( repeat_block_holder );
        // reinitialize colorpickers
        if ( typeof jscolor !== 'undefined' )
        {
            jscolor.init();
        }
        // reload sortables entirely to fix problem with compound field containment
        // ...alas, .sortable('refresh') doesn't cut it.
        load_sortable();
    });
    
    // Jumps around when moving large boxes...
    // difficulty capturing all scenarios with mousedown and mouseup;
    // start event doesn't trigger before the element is removed...
    // difficult problem, apparently.  Could resolve with window resize callback
    // to reset height to auto, but keep that as a last resort... surely
    // there is a cleverer solution?
    function load_sortable() {
        jQuery( '.repeat_block_holder' ).sortable({
            items: '.repeat_block',
            axis: 'y',
            cursor: "move",
            cursorAt: { left: 0, top:0 },
            start: function( event, ui ) {
                //jQuery( this ).css( 'height', jQuery( this ).height() );
            },
            handle: '.repeat_box_handle', // consider adding a handle in the future...
            create: function() {
                jQuery( this ).children( '.repeat_block' ).each( function(){
                    jQuery( this ).prepend( '<span class="repeat_box_handle"> <span class="icon">&#8597</span></span><span class="up">&#x25B2;</span><span class="down">&#x25BC;</span>' );
                });
            },
            update: function( event, ui ) {
                reset_numbers( jQuery( this ) );
                //jQuery( this ).css( 'height', 'auto');
            },
            containment: 'parent'
        });
    }
    
    jQuery( '.repeat_block_holder' ).on( 'click', '.repeat_block .up', function(event){
        event.stopPropagation();
        $this = jQuery( this );
        var $repeat_block = $this.closest( '.repeat_block' );
        var $repeat_block_holder = $this.closest( '.repeat_block_holder' );
        var $repeat_blocks = $repeat_block_holder.children( '.repeat_block' ); 
        var index = $repeat_blocks.index( $repeat_block );
        
        if ( $repeat_blocks.length > 1 && index > 0 ) {
            var $next = $repeat_blocks.eq( index - 1 );
            // disable any running animations
            $next.stop( true, true );
            $repeat_block.stop( true, true );
            // use clone to eliminate sticky hover effect on up button
            var $clone = $repeat_block.clone();
            $next.before( $clone);
            $repeat_block.remove();
            reset_numbers( $repeat_block_holder );
            $clone.effect( 'highlight', {color:'yellow'}, 1200);
            $next.effect( 'highlight', {color:'#ddd'}, 700);
        }
        /**/
    } );
    jQuery( '.repeat_block_holder' ).on( 'click', '.repeat_block .down', function(event){
        event.stopPropagation();
        $this = jQuery( this );
        var $repeat_block = $this.closest( '.repeat_block' );
        var $repeat_block_holder = $this.closest( '.repeat_block_holder' );
        var $repeat_blocks = $repeat_block_holder.children( '.repeat_block' ); 
        var index = $repeat_blocks.index( $repeat_block );
        if ( $repeat_blocks.length > 0 && index < $repeat_blocks.length -1 ) {
            var $next = $repeat_blocks.eq( index + 1 );
            // disable any running animations
            $next.stop( true, true );
            $repeat_block.stop( true, true );
            // use clone to eliminate sticky hover effect on down button
            var $clone = $repeat_block.clone();
            $next.after( $clone);
            $repeat_block.remove();
            reset_numbers( $repeat_block_holder );
            $clone.effect( 'highlight', {color:'yellow'}, 1200 );
            $next.effect( 'highlight', {color:'#ddd'}, 700 );
        }
        /**/
    } );
    
    load_sortable();
});