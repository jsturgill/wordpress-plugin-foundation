    function set_datepicker(target)
    {
        //if (!jQuery(target).hasClass('hasJdsCoreDatepicker')) 
        //{
            jQuery(target).datepicker({
                changeYear: true,
                changeMonth: true,
                showButtonPanel: true,
                yearRange: '-2:+2',
                showOn: 'focus', 
                buttonText: "select date",
                //markerClassName: 'hasJdsCoreDatepicker'
            }).next(".ui-datepicker-trigger").addClass("button jds_core_button");
        //}
    }
    jQuery(document).ready(function() {
        jQuery(document).on('focus', ".jds_core_datepicker", function(){
            set_datepicker(this);
        });
        jQuery(".jds_core_datepicker").each(function(){
            set_datepicker(this);
        });
    });